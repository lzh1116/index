<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
	<title><?php echo ($title); ?></title>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="/Public/Home/css/style.css"/>
	<script src="/Public/Common/js/jquery.min.js"></script>
</head>
<body>
<div class="top">
	<div class="top-nav">
	<ul><li>收藏本站</li><li>关注本站</li></ul>
	<ul class="right">
	<?php if(isset($userinfo["id"])): ?><li><?php echo ($userinfo["name"]); ?>，欢迎来到清新商城！[<a href="<?php echo U('User/logout');?>">退出</a>]<li>
	<?php else: ?>
		<li>您好，欢迎来到清新商城！[<a href="<?php echo U('User/login');?>">登陆</a>][<a href="<?php echo U('User/register');?>">免费注册</a>]</li><?php endif; ?>
		<li class="line">|</li><li><a href="<?php echo U('Order/index');?>">我的订单</a></li>
		<li class="line">|</li><li><a href="<?php echo U('User/index');?>">会员中心</a></li>
		<li class="line">|</li><li><a href="<?php echo U('Cart/index');?>">我的购物车</a></li>
		<li class="line">|</li><li>联系客服</li>
	</ul>
	</div>
</div>
<div class="box">
	<div class="header">
		<a class="left" href="<?php echo U('Index/index');?>"><div class="logo"></div></a>
		<div class="search left">
			<input type="text" class="left" />
			<input class="search-btn" type="button" value="搜索" />
			<p class="search-hot">热门搜索：PHP培训　专业教材　智能手机　平板电脑</p>
		</div>
		<div class="info left">
			<input type="button" value="会员中心" onclick="location.href='<?php echo U('User/index');?>'" />
			<input type="button" value="去购物车结算" onclick="location.href='<?php echo U('Cart/index');?>'" />
		</div>
	</div>
	<div class="nav">
		<ul><li id="Index_find"><a class="category" href="<?php echo U('Index/find');?>">全部商品分类</a></li>
		<li id="Index_index"><a href="/">首页</a></li>
			<li><a href="#">特色购物</a></li><li><a href="#">优惠促销</a></li><li><a href="#">限时秒杀</a></li>
			<li><a href="#">品牌专区</a></li><li><a href="#">服务中心</a></li>
		</ul>
	</div>

	
	<div class="usercenter">
<ul class="menu left">
	<li><a href="<?php echo U('User/index');?>" id="User_index">个人信息</a></li>
	<li><a href="<?php echo U('Order/index');?>" id="Order_index">我的订单</a></li>
	<li>我的关注</li>
	<li><a href="<?php echo U('User/addr');?>" id="User_addr">收货地址</a></li>
	<li>消费记录</li>
	<li><a href="<?php echo U('Cart/index');?>" id="Cart_index">购物车</a></li>
</ul>
<script>
$("<?php echo (CONTROLLER_NAME); ?>_<?php echo (ACTION_NAME); ?>").addClass("curr");
</script>
	

	<div class="content left">我的购物车
		<form method="post" action="<?php echo U('Order/buy',array('cart'=>1));?>">
		<table border="1" class="shopcart">
			<tr class="tr-tit"><th class="w50">
			<a href="javascript:void(0);" onclick="checkedAll()">全选</a></th>
			<th>商品</th><th class="w50">单价</th><th class="w92">数量</th><th class="w50">操作</th></tr>
			<?php if(is_array($cart)): foreach($cart as $key=>$v): ?><tr class="item">
				<td class="center"><input type="checkbox" class="check" 
				name="id[]" value="<?php echo ($v["goods_id"]); ?>" /></td>
				<td><a href="<?php echo U('Index/goods',array('id'=>$v['goods_id']));?>" 
				target="_blank"><?php echo ($v["name"]); ?></a></td>
				<td class="center"><span class="item-price"><?php echo ($v["price"]); ?>.00</span></td>
				<td><input class="setNum" type="button" value="-" />
				<input class="item-num" onkeyup="checkNum(this)" type="text" 
				value="<?php echo ($v["num"]); ?>" name="num[]"/>
				<input class="setNum"  type="button" value="+" /></td>
				<td class="center"><a href="<?php echo U('Cart/del',array('id'=>$v['id']));?>" />删除</a></td>
			</tr><?php endforeach; endif; ?>			<tr><th><a href="javascript:void(0);" onclick="checkedAll()">全选</a></th>
			<td colspan="4">删除选中的商品&nbsp;&nbsp;继续购物&nbsp;&nbsp;
				共<span id="num"></span>件商品 总计：<span class="price">￥<span id="monery"></span></span>
				<input type="submit" value="提交订单" class="order-btn" />
			</td></tr>
		</table>
		</form>
	</div>
	<div class="clear"></div>
</div>
<script>
	//点击修改数量
	$(".setNum").click(function () {
		if ($(this).val() === '-') {
			if ($(this).next().val() !== '1') {
				var num = $(this).next().val() - 1;
				$(this).next().attr("value", num);
				$(this).next().val(num);
			}
		}else if ($(this).val() === '+') {
			if ($(this).prev().val() !== '100') {
				var num = parseInt($(this).prev().val()) + parseInt(1);
				$(this).prev().attr("value", num);
				$(this).prev().val(num);
			}
		}
		func();
	});
	//键盘修改数量
	function checkNum(obj) {
		//判断当前值是否合法   凡是不合法的都重置为1
		var num = $(obj).val();
		if (num <= 1 || num >= 100) {
			$(obj).val(1);
		}
		func();
	}
	//默认情况下，设置为全选状态
	$(function () {
		$(":checkbox").prop("checked", true);
		$(":checkbox").attr("checked", true);
		func();
	});
	//全选
	function checkedAll() {
		$(":checkbox").each(function () {
			if (this.checked) {
				$(this).prop("checked", false);
				$(this).attr("checked", false);
			} else {
				$(this).prop("checked", true);
				$(this).attr("checked", true);
			}
		});
		func();
	}
	//单个选择时的状态设置
	$(".check").click(function () {
		$(this).each(function (i, v) {
			if (!v.checked) {
				$(this).prop("checked", false);
				$(this).attr("checked", false);
			} else {
				$(this).prop("checked", true);
				$(this).attr("checked", true);
			}
		});
		func();
	});
	//计算总价
	function func() {
		var price = 0;
		var num = 0;
		var totalnum = 0;
		$(".item").find(":checkbox:checked").each(function () {
			$(this).closest("tr").find(".item-num").each(function () {
				totalnum += parseInt($(this).val());
				num = parseInt($(this).val());
				$(this).closest("tr").find(".item-price").each(function () {
					price += parseInt($(this).text()) * num;
				});
			});
		});
		$("#monery").text(price);
		$("#num").text(totalnum);
	}
</script>


	
	
	<div class="service">
		<ul><li>购物指南</li><li>配送方式</li><li>支付方式</li>
			<li>售后服务</li><li>特色服务</li><li>网络服务</li>
		</ul>
	</div>
	<div class="footer">清新商城·本项目仅供学习使用</div>
</div>
</body>
</html>