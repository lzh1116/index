<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
	<title><?php echo ($title); ?></title>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="/Public/Home/css/style.css"/>
	<script src="/Public/Common/js/jquery.min.js"></script>
</head>
<body>
<div class="top">
	<div class="top-nav">
	<ul><li>收藏本站</li><li>关注本站</li></ul>
	<ul class="right">
	<?php if(isset($userinfo["id"])): ?><li><?php echo ($userinfo["name"]); ?>，欢迎来到清新商城！[<a href="<?php echo U('User/logout');?>">退出</a>]<li>
	<?php else: ?>
		<li>您好，欢迎来到清新商城！[<a href="<?php echo U('User/login');?>">登陆</a>][<a href="<?php echo U('User/register');?>">免费注册</a>]</li><?php endif; ?>
		<li class="line">|</li><li><a href="<?php echo U('Order/index');?>">我的订单</a></li>
		<li class="line">|</li><li><a href="<?php echo U('User/index');?>">会员中心</a></li>
		<li class="line">|</li><li><a href="<?php echo U('Cart/index');?>">我的购物车</a></li>
		<li class="line">|</li><li>联系客服</li>
	</ul>
	</div>
</div>
<div class="box">
	<div class="header">
		<a class="left" href="<?php echo U('Index/index');?>"><div class="logo"></div></a>
		<div class="search left">
			<input type="text" class="left" />
			<input class="search-btn" type="button" value="搜索" />
			<p class="search-hot">热门搜索：PHP培训　专业教材　智能手机　平板电脑</p>
		</div>
		<div class="info left">
			<input type="button" value="会员中心" onclick="location.href='<?php echo U('User/index');?>'" />
			<input type="button" value="去购物车结算" onclick="location.href='<?php echo U('Cart/index');?>'" />
		</div>
	</div>
	<div class="nav">
		<ul><li id="Index_find"><a class="category" href="<?php echo U('Index/find');?>">全部商品分类</a></li>
		<li id="Index_index"><a href="/">首页</a></li>
			<li><a href="#">特色购物</a></li><li><a href="#">优惠促销</a></li><li><a href="#">限时秒杀</a></li>
			<li><a href="#">品牌专区</a></li><li><a href="#">服务中心</a></li>
		</ul>
	</div>

	
	<div class="usercenter">
<ul class="menu left">
	<li><a href="<?php echo U('User/index');?>" id="User_index">个人信息</a></li>
	<li><a href="<?php echo U('Order/index');?>" id="Order_index">我的订单</a></li>
	<li>我的关注</li>
	<li><a href="<?php echo U('User/addr');?>" id="User_addr">收货地址</a></li>
	<li>消费记录</li>
	<li><a href="<?php echo U('Cart/index');?>" id="Cart_index">购物车</a></li>
</ul>
<script>
$("<?php echo (CONTROLLER_NAME); ?>_<?php echo (ACTION_NAME); ?>").addClass("curr");
</script>
	<div class="content left">管理收货地址
<form method="post">
		<input id="address" type="hidden" value="" name="address" />
		<table border="1">
			<tr><th>收件人：</th><td><input type="text" value="<?php echo ($addr["consignee"]); ?>" name="consignee" /></td></tr>
			<tr><th>收件地区：</th><td>
				<select id="province" onchange="toCity()">
				<?php if(!empty($addr["area"]["0"])): ?><option><?php echo ($addr["area"]["0"]); ?></option><?php endif; ?></select>
				<select id="city" onchange="toArea()"><option><?php echo ($addr["area"]["1"]); ?></option></select>
				<select id="area"><option><?php echo ($addr["area"]["2"]); ?></option></select>
				</td></tr>
			<tr><th>详细地址：</th><td><input id="addr" type="text" value="<?php echo ($addr["area"]["3"]); ?>" /></td></tr>
			<tr><th>手机：</th><td><input type="text" value="<?php echo ($addr["phone"]); ?>" name="phone" /></td></tr>
			<tr><th>邮箱：</th><td><input type="text" value="<?php echo ($addr["email"]); ?>" name="email" /></td></tr>
			<tr><td colspan="2" class="button center"><input type="submit" value="保存" /> <input type="reset" value="重置" /></td></tr>
		</table>
		</form>
	</div>
</div>
<script>
//在加载事件中载入省份
var xmldom = null;	//保存请求到的xml文档信息
$("#province").append("<option value=0>--请选择--</option>");
$(function () {
	$.ajax({ //利用ajax去服务器端请求xml信息
		url: '/Public/Common/js/ChinaArea.xml',
		dataType: 'xml',
		type: 'get',
		success: function (msg) {
			xmldom = msg;
			//msg会以xmldom文档节点对象返回
			var province = $(msg).find('province');
			province.each(function () {
				var name = $(this).attr('province');  //获得省份名称
				var id = $(this).attr('provinceID'); //省份id信息
				$("#province").append("<option value='" + id + "'>" + name + "</option>");
			});
		}
	});
});
//通过onchange内容改变事件达到“省份和城市”关联效果
function toCity() {
	//获得被切换的省份id信息
	var pid = $("#province").val();
	pid = pid.substr(0, 2);//获得value的前两位信息
	//获得city信息，属性cityID的前两位是pid开始
	var city = $(xmldom).find("City[CityID^=" + pid + "]");
	$("#city").empty();
	$("#city").append("<option value=0>--请选择--</option>");
	$("#area").empty();
	$("#area").append("<option value=0>--请选择--</option>");
	//遍历city信息，赋值到select下拉列表中
	city.each(function () {
		var name = $(this).attr('City');
		var id = $(this).attr('CityID');
		$("#city").append("<option value='" + id + "'>" + name + "</option>");
	});
}
function toArea() {
	var pid = $("#city").val();
	pid = pid.substr(0, 4);
	//获得city信息，属性cityID的前两位是pid开始
	var area = $(xmldom).find("Piecearea[PieceareaID^=" + pid + "]");
	$("#area").empty();
	$("#area").append("<option value=0>--请选择--</option>");
	area.each(function () {
		var name = $(this).attr('Piecearea');
		var id = $(this).attr('PieceareaID');
		$("#area").append("<option value='" + id + "'>" + name + "</option>");
	});
}
//提交表单时检查并拼接完整地址
$("form").submit(function(){
	var pro_val = $("#province").find("option:selected").text();
	var city_val = $("#city").find("option:selected").text();
	var area_val = $("#area").find("option:selected").text();
	var addr = $("#addr").val();
	if(pro_val === '--请选择--' || city_val === '--请选择--' || area_val === '--请选择--' || $.trim(addr)===''){
		alert('请输入正确的地址');
		return false;
	}
	$("#address").val(pro_val+','+city_val+','+area_val+','+addr);
});
</script>

	
	
	<div class="service">
		<ul><li>购物指南</li><li>配送方式</li><li>支付方式</li>
			<li>售后服务</li><li>特色服务</li><li>网络服务</li>
		</ul>
	</div>
	<div class="footer">清新商城·本项目仅供学习使用</div>
</div>
</body>
</html>