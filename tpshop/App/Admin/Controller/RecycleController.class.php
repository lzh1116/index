<?php
namespace Admin\Controller;
class RecycleController extends CommonController{
    //显示回收站商品列表
    public function index(){
        $p=I('get.p/d',0);
        $goods=D('goods');
        $data['goods']=$goods->getList('recycle',-1,$p);
        if(empty($data['goods']['data']) && $p>0){
            $this->redirect('Recycle/index');
        }
        //print_r($data);
        $data['p']=$p;
        $this->assign($data);
        $this->display();
    }
    public function rec(){//恢复商品
        if(!IS_POST){
            $this->error('恢复失败：未选择商品');//阻止直接访问
        }
        $p=I('get.p/d',0);
        $id=I('post.id/d',0);
        $jump=U('Recycle/index',array('p'=>$p));
        $goods=M('goods');
        if(!$goods->autoCheckToken($_POST)){
            $this->error('表单已过期，请重新提交',$jump);
        }
        //将商品取消删除
        if(false===$goods->where(array('id'=>$id))->save(array('recycle'=>'no'))){
            $this->error('恢复商品失败',$jump);
        }
        redirect($jump);//恢复成功，跳转
    }
    public function del(){//彻底删除商品
        if(!IS_POST){
            $this->error('删除失败：未选择商品');
        }
        $p=I('get.p/d',0);
        $id=I('post.id/d',0);
        $jump=U('Recycle/index',array('p'=>$p));
        $goods=D('goods');
        if(!$goods->autoCheckToken($_POST)){
            $this->error('表单已过期，请重新提交',$jump);
        }
        $where=array('id'=>$id,'recycle'=>'yes');
        //删除商品图片
        $goods->delThumbFile($where);
        //删除商品数据
        $goods->where($where)->delete();
        //删除成功，跳转
        redirect($jump);
    }
    
    
    
}


