<?php
namespace Admin\Controller;

class CategoryController extends CommonController {
        public function index(){//分类列表
            
          // print_r(D('category')->check_pid());die();
            $data=D('category')->getList();
            $this->assign('category',$data);
            $this->display();           
        }
        public function add(){//添加分类
            $category=D('category');//
            if(IS_POST){
                if(!$category->create()){//创建数组对象
                    $this->error('添加失败：'.$category->getError());
                }
                if(!$category->add()){//添加到数据库
                    $this->error('添加失败：保存到数据库失败。');
                }
                if(isset($_POST['return'])){
                    $this->redirect('Category/index');
                   
                }
                $this->assign('parent_id',I('post.parent_id/d',0));//
                $this->assign('success',true);
            }
            //获取分类数据
            $data=$category->getList();
            $this->assign('category',$data);
            $this->display();
        }
        public function edit(){
            $cate_id=I('get.cate_id/d',0);//待修改分类的id，/d用于转为整型
            $category=D('category');
            if(IS_POST){
                //检查父级分类是否合法
                if(in_array(I('post.parent_id/d'),$category->getSubIds($id))){
                    $this->error('不允许将父级分类修改为本身或子孙分类。');
                }
                //创建数组对象
                if(!$category->create(null,2)){
                    $this->error('修改失败：'.$category->getError());
                }
                //保存到数据库
                if(false===$category->where(array('cate_id'=>$cate_id))->save()){
                    $this->error('修改失败:保存到数据库失败。');
                }
                //保存成功
                $this->redirect('Category/index');
            }
            //根据id查询分类信息
            $data=$category->field('cate_id,cate_name,parent_id')->where(array('cate_id'=>$cate_id))->find();
            if(!$data){
                $this->error('修改失败：分类不存在。');
            }
            $data['category']=$category->getList();//分类列表
            $this->assign($data);
            $this->display();
        }
        public function del(){
            //阻止直接访问
            if(!IS_POST){
                $this->error('删除失败：未选择分类。');
            }
            $cate_id=I('post.cate_id/d',0);//待删除的分类id
            $jump=U('Category/index');
            $category=M('category');
            //判断是否存在子类
            if($category->where(array('parent_id'=>$cate_id))->getField('cate_id')){
                $this->error('删除失败，只允许删除最底层分类！');
            }
            //检查表单令牌
            if(!$category->autoCheckToken($_POST)){
                $this->error('表单已过期，请重新提交',$jump);
            }
            //删除分类
            if(!$category->where(array('cate_id'=>$cate_id))->delete()){
                $this->error('删除分类失败',$jump);
            }
            //将该分类下的商品设置为未分类
            M('Goods')->where(array('cate_id'=>$cate_id))->save(array('cate_id'=>0));
            //删除成功，跳转到分类列表
            redirect($jump);
        }
        
        
        
}






















