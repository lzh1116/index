<?php
namespace Admin\Controller;
use Think\Controller;
class CommonController extends Controller {
    
    
        public function __construct(){
            parent::__construct();//先执行父类的构造方法
            $this->checkUser();//登陆检查
            //已经登陆，为模板分配用户名变量
            $this->assign('adminname',session('userinfo.name'));            
        }
        
      /*   public function _initialize(){
            $this->checkUser();//登陆检查
            //已经登陆，为模板分配用户名变量
            $this->assign('adminname',session('userinfo.name'));
        } */
        
        //检查用户是否已经登陆
        private function checkUser(){
            if(!session('?userinfo')){
                $this->redirect('Login/index');//未登录，先登录
            }
        }
        public function _empty($name){
            $this->error('无效的操作:'.$name);
        }
}