<?php
namespace Admin\Controller;
class GoodsController extends CommonController{
    public function index(){
        //获取参数
        $p=I('get.p/d',0);//当前页码
        $cid=I('get.cid/d',-1);//分类id（0表示未分类，-1表示全部分类）
        $goods=D('goods');
        $category=D('category');
        //如果分类id大于0，则取出所有子分类id
        $cids=($cid>0)?$category->getSubIds($cid):$cid;
        //获取商品列表
        $data['goods']=$goods->getList('goods',$cids,$p);
        //防止空页被访问
        if(empty($data['goods']['data']) && $p>0){
            $this->redirect('Goods/index',array('cid'=>$cid));
        }
        $data['category']=$category->getList();//查询分类列表
        $data['cid']=$cid;
        $data['p']=$p;
        $this->assign($data);
        $this->display();
    }
    public function add(){
        $cid=I('get.cid/d',0);
        if($cid<0){
            $cid=0;
        }
        $category=D('category');
        $goods=D('goods');
        if(IS_POST){
            if(!$goods->create()){//创建数组对象
                $this->error('添加商品失败:'.$goods->getError());
            }
            //处理特殊字段
            $goods->cate_id=$cid;//商品分类
            $goods->thumb='';//商品预览图
            $goods->desc=I('post.desc','','htmlpurifier');//商品描述文本过滤
            //如果有图片上传，则上传并生成预览图
            if(isset($_FILES['thumb']) && $_FILES['thumb']['error']===0){
                $rst=$goods->uploadThumb('thumb');//上传并生成预览图
                if(!$rst['flag']){
                    $this->error('上传图片失败：'.$rst['error']);
                }
                $goods->thumb=$rst['path'];//上传成功，保存文件路径
            }
            if(!$goods->add()){
                $this->error('添加商品失败');
            }
            //添加商品成功
            if(isset($_POST['return'])){
                $this->redirect('Goods/index');
                $this->assign('success',true);
            }
        }
        //查询分类列表
        $data['category']=$category->getList();
        $data['cid']=$cid;
        $this->assign($data);
        $this->display();
    }
    public function edit(){
        $id=I('get.id/d',0);//待修改商品id
        $p=I('get.p/d',0);//当前页码
        $cid=I('get.cid/d',0,'abs');//待修改商品的分类id
        $category=D('category');
        $goods=D('goods');
        $where=array('id'=>$id,'recycle'=>'no');
        if(IS_POST){
            if(!$goods->create()){
                $this->error('修改商品失败:'.$goods->getError());
            }
            $goods->cate_id=$cid;//保存商品分类
            $goods->desc=I('post.desc','','htmlpurifier');
            //若有预览图文件上传，则更新预览图
            if(isset($_FILES['thumb']) && $_FILES['thumb']['error']===0){
                $rst=$goods->uploadThumb('thumb');
                if(!$rst['flag']){
                    $this->error('上传图片失败:'.$rst['error']);
                }
                $goods->thumb=$rst['path'];//上传成功，保存文件路径
                $goods->delThumbFile($where);//删除商品图片
            }
            //保存到数据库
            if(false===$goods->where($where)->save()){
                $this->error('修改商品失败');
            }
            //修改商品成功
            if(isset($_POST['return'])){
                $this->redirect('Goods/index',array('cid'=>$cid,'p'=>$p));
            }
            $this->assign('success',true);
        }
        //查询商品数据
        $data['goods']=$goods->getGoods($where);
        if(!$data['goods']){
            $this->error('修改失败：商品不存在');
        }
        //查询分类列表
        $data['category']=$category->getList();
        $data['cid']=$cid;
        $data['id']=$id;
        $data['p']=$p;
        
        $this->assign($data);
        $this->display();
    }
    public function del(){//删除商品，放入回收站
        //阻止直接访问
        if(!IS_POST){
            $this->error('删除失败：未选择商品');
        }
        //获取参数
        $cid=I('get.cid/d',0);
        $p=I('get.p/d',0);//当前页码
        $id=I('post.id/d',0);//待处理的商品id
        $jump=U('Goods/index',array('cid'=>$cid,'p'=>$p));
        $goods=M('goods');
        //检查表单令牌
        if(!$goods->autoCheckToken($_POST)){
            $this->error('表单已过期，请重新提交',$jump);
        }
        //将商品放入回收站
        if(false===$goods->where(array('id'=>$id))->save(array('recycle'=>'yes'))){
            $this->error('删除商品失败',$jump);
        }
        redirect($jump);//删除成功，跳转
    }
    public function change(){
        //阻止直接访问
        if(!IS_POST){
            $this->error('操作失败：未选择商品');
        }
        //获取参数
        $cid=I('get.cid/d',0);//分类id
        $p=I('get.p/d',0);//当前页码
        $id=I('post.id/d',0);//待处理的商品id
        $field=I('post.field');//待处理的字段
        $status=I('post.status');//待处理的字段值
        $jump=U('Goods/index',array('cid'=>$cid,'p'=>$p));
        $goods=M('goods');
        //检测输入变量
        if($field != 'on_sale' && $field != 'recommend'){
            $this->error('操作失败：非法字段');
        }
        if($status!='yes' && $status!='no'){
            $this->error('操作失败：非法状态值');
        }
        //检查表单令牌
        if(!$goods->autoCheckToken($_POST)){
            $this->error('表单已过期，请重新提交',$jump);
        }
        //
        if(false===$goods->where(array('id'=>$id,'recycle'=>'no'))->save(array($field=>$status))){
            $this->error('操作失败：数据库保存失败',$jump);
        }
        redirect($jump);//操作成功，跳转
    }
    //商品详情    在线编辑器   图片上传
    public function uploadImage(){
        $savePath='./Public/Uploads/desc';//上传目录
        $config=array(//上传配置
            'savePath'=>$savePath,//存储文件夹
            'subPath'=>date('Y-m/d'),//子目录
            'allowFiles'=>array('.jpg','.jpeg','.png','.gif')
        );
        //实例化UMEditor配套的文件上传类
        $upload=new \Components\Uploader('upfile',$config);
        //返回JSON数据给UMEditor
        $type=$_REQUEST['type'];
        $callback=$_GET['callback'];
        $info=$upload->getFileInfo();
        $info=$callback ? "<script>$callback(".json_encode($info).')</script>':json_encode($info);
        $this->ajaxReturn($info,'EVAL');
    }
    
 
    
    

    
}





















