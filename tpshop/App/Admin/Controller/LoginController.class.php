<?php
namespace Admin\Controller;
use Think\Controller;
class LoginController extends Controller {
    public function index(){
        if(IS_POST){
            $admin=D('admin');
            if(!$admin->create()){
                $this->error('登陆失败：'.$admin->getError());
                return ;
            }else{
                
                $verify=I('post.verify');
                if($this->check_verify($verify)){
                    //验证用户名，密码
                    $name=I('post.adminname');
                    $password=MD5(I('post.password'));
                    $admin=M('admin')->where(array('adminname'=>$name))->field('password,id')->find();
                    if($password==$admin['password']){
                        
                        session('userinfo',array('name'=>$name,'id'=>$admin['id']));
                        $this->success('登陆成功',U('Index/index'));
                        return ;
                    }else {
                        $this->error('用户名或密码错误');
                    }
                    
                }else{
                    $this->error('验证码错误');
                }
            }          
        }
        $this->display();
    }
    
    
    
    public function getVerify() {
        
        $Verify = new \Think\Verify();
        $Verify->fontSize = 30;
        $Verify->length   = 4;
        //$Verify->useNoise = false;
        $Verify->useCurve=false;//干扰线
        $Verify->entry();
    }
    
    private function check_verify($code, $id = ''){
        $verify = new \Think\Verify();
        return $verify->check($code, $id);
    }
    public function logout(){
        session(null);//清空后台所有会话
        $this->redirect('Login/index');
    }
    
    
   
}   
    










