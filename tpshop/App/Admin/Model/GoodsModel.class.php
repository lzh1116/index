<?php
namespace Admin\Model;
use Think\Model;

class GoodsModel extends Model{
    //表单字段过滤
    protected $insertFields='sn,name,price,stock,on_sale,recommend';
    protected $updateFields='sn,name,price,stock,on_sale,recommend';
    //
    protected $_validate=array(
        array('name','1,40','商品名称不合法(1-40个字符)',1,'length'),
        array('sn','/^[0-9A-Za-z]{1,10}$/','商品编号不合法(1-10个字符)',1),
        array('on_sale',array('yes','no'),'on_sale字段填写错误',1,'in'),
        array('recommend',array('yes','no'),'recommend字段 填写错误',1,'in'),
        array('price','0.01,100000','商品价格输入不合法(0.01~100000)',1,'between'),
        array('stock','0,900000','商品库存输入不合法',1,'between'),
    );
    
    public function getList($type='goods',$cids=0,$p=0){
        $order='g.id desc';//排序条件
        $field='c.cate_name as cate_name,g.cate_id,g.id,g.name,g.on_sale,g.stock,g.recommend';
        if($type=='goods'){//商品列表页获取数据时
            $where=array('g.recycle'=>'no');
        }elseif ($type=='recycle'){//商品回收站获取数据时
            $where=array('g.recycle'=>'yes');
        }
        //
        if($cids==0){
            $where['g.cate_id']=0;
        }elseif ($cids>0){
            $where['g.cate_id']=array('in',$cids);
        }
        //分页
        $pagesize=C('USER_CONFIG.pagesize');
        $count=$this->alias('g')->where($where)->count();//获取商品总数
        $page=new \Think\Page($count,$pagesize);//实例化分页类
        $this->_customPage($page);//分页类样式
        //查询数据
        $data=$this->alias('g')->join('__CATEGORY__ as c on c.cate_id=g.cate_id','left')->
        field($field)->where($where)->order($order)->page($p,$pagesize)->select();
        //返回结果
        return array(
            'data'=>$data,//商品列表数组
            'pagelist'=>$page->show(),//分页链接html
        );
    }
    private function _customPage($page){
        $page->lastSuffix=false;
        $page->setConfig('prev','上一页');
        $page->setConfig('next','下一页');
        $page->setConfig('first','首页');
        $page->setConfig('last','末页');
        $page->setConfig('theme','条数： %TOTAL_ROW% 条     %FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END%');
    }
    public function getGoods($where){//根据where条件查询商品数据
        $field='id,cate_id,sn,name,price,thumb,stock,on_sale,recommend,desc';
        return $this->field($field)->where($where)->find();
    }
    //根据where条件删除商品预览图文件
    public function delThumbFile($where){
        //取出原图文件名
        $thumb=$this->where($where)->getField('thumb');
        if(!$thumb){
            return ;
        }
        $path="./Public/Uploads/big/$thumb";//大图路径
        if(is_file($path)){
            unlink($path);//删除大图路径
        }
        $path="./Public/Uploads/small/$thumb";
        if(is_file($path)){
            unlink($path);
        }//会残留空目录，可以定期清理
    }
    //上传预览图片并生成缩略图
    //返回数组（flag=是否执行成功，error=失败时的错误信息，path=成功时的保存路径）
    public function uploadThumb($upfile){
        //准备上传目录
        $file['temp']='./Public/Uploads/temp/';//准备临时目录
        file_exists($file['temp']) or mkdir($file['temp'],0777,true);
        //上传文件
        $upload=new \Think\Upload(array(
           'exts'=>array('jpg','jpeg','png','gif'),//允许的文件名后缀
           'rootPath'=>$file['temp'],//文件保存路径
           'autoSub'=>false,//不生成子目录
        ));
        if(false===($rst=$upload->uploadOne($_FILES[$upfile]))){
            //上传失败时返回错误信息
            return array('flag'=>false,'error'=>$upload->getError());
        }
        //准备生成缩略图
        $file['name']=$rst['savename'];//文件名
        $file['save']=date('Y-m/d/');//子目录
        $file['path1']='./Public/Uploads/big/'.$file['save'];//大图路径
        $file['path2']='./Public/Uploads/small/'.$file['save'];//小图路径
        //创建保存目录
        file_exists($file['path1']) or mkdir($file['path1'],0777,true);
        file_exists($file['path2']) or mkdir($file['path2'],0777,true);
        //生成缩略图
        $image=new \Think\Image();
        $image->open($file['temp'].$file['name']);//打开文件
        $image->thumb(350, 300,2)->save($file['path1'].$file['name']);//保存大图
        $image->open($file['temp'].$file['name']);
        $image->thumb(220,220,2)->save($file['path2'].$file['name']);//保存小图
        unlink($file['temp'].$file['name']);//删除临时文件
        //返回文件路径
        return array('flag'=>true,'path'=>$file['save'].$file['name']);
    }
    //插入数据前置操作
    protected function _before_insert(&$data, $option){
        $data['recycle']='no';//新商品是未删除的
        $data['add_time']=time();
        $data['price']=(float)$data['price'];        
    }

    
    
    
    
    
}















