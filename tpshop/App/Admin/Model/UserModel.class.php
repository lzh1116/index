<?php
namespace Admin\Model;
use Think\Model;

class UserModel extends Model{
    public function getList(){//获取会员列表
        $field='user_id,user_name,user_phone,user_email';
        $where=array();
        $order='user_id desc';
        $count=$this->where($where)->count();//查询数据
        $page=new \Think\Page($count,C('USER_CONFIG.pagesize'));
        $this->_customPage($page);//分页类样式
        $limit=$page->firstRow.','.$page->listRows;
        
        return array(//获取数据
            'list'=>$this->field($field)->where($where)->order($order)->limit($limit)->select(),
            'page'=>$page->show(),
        );
    }
    private function _customPage($page){
        $page->lastSuffix=false;
        $page->setConfig('prev','上一页');
        $page->setConfig('next','下一页');
        $page->setConfig('first','首页');
        $page->setConfig('last','尾页');        
    }
   
    
}

