<?php
namespace Admin\Model;
use Think\Model;

class AdminModel extends Model{
    
    protected $_validate = array(
        array('verify','require','验证码必须！'), //默认情况下用正则进行验证
        array('adminname','/^\w{4,10}$/','用户名不合法(4-10位数字字母下划线组成)！',1,'regex'),
        // 在新增的时候验证name字段是否唯一
        array('password','/^\w{5,10}$/','密码不合法(5-10位数字字母下划线组成)！',1,'regex'), 
        // 在新增的时候验证password字段是否唯一
        
    );
    
    
}