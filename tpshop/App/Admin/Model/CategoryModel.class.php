<?php
namespace Admin\Model;
use Think\Model;

class CategoryModel extends Model{
    protected $insertFields='cate_name,parent_id';
    protected $updateFields='cate_name,parent_id';
    //自动验证
    protected $_validate=array(
        array('parent_id','require','父级分类不能为空',self::MUST_VALIDATE),
        array('cate_name','require','分类名不能为空',self::MUST_VALIDATE),
    );
    //自动完成
    protected $_auto=array(
        array('parent_id','check_pid',3,'callback'),
    );      
    public function check_pid($pid){//验证上传的value值
        $pids=$this->field('cate_id')->select();
        $new=array();
        foreach ($pids as $v){
           array_push($new, $v['cate_id']);
        }               
        if(!in_array($pid, $new)){
            return  0;
        }        
       return $pid;        
    }
    
    
    //查询分类数据
    private function getData(){
        static $data=null;//缓存查询结果
        if(!$data){
            $data=$this->field('cate_id,cate_name,parent_id')->select();
            
        }   
        return $data;
    }
    //获取分类列表
    public function getList(){
        category_list($this->getData(),$data);
        return $data;
    }
    //查找所有的子孙分类
    public function getSubIds($id){
        $data=array($id);//将ID自身放入数组头部
        category_child($this->getData(),$data,$id);
        return $data;
    }
    
    
    
}














