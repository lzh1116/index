<?php
//自定义的方法
//获取一维数组分类列表
function category_list($data,&$rst,$parent_id=0,$level=0){
    foreach ($data as $v){
        if($v['parent_id']==$parent_id){
            $v['level']=$level;//保存分类级别
            $rst[]=$v;//保存符合条件的元素
            category_list($data, $rst,$v['cate_id'],$level+1);//递归
        }
    }
}
//根据任意分类id查找子孙分类id
function category_child($data,&$rst,$id=0){
    foreach ($data as $v){
        if($v['parent_id']==$id){
            $rst[]=(int)$v['cate_id'];
            category_child($data, $rst,$v['cate_id']);
        }
    }
}
//把分类转为多维数组
function category_tree($data,$parent_id=0,$level=0){
    $temp=$rst=array();
    foreach ($data as $v){
        $temp[$v['cate_id']]=$v;
    }
    foreach ($temp as $v){
        if(isset($temp[$v['parent_id']])){
            $temp[$v['parent_id']]['child'][]=&$temp[$v['cate_id']];
        }else{
            $rst[]=&$temp[$v['cate_id']];
        }
    }
    return $rst;
}
//查找分类的家谱
function category_family($data, $id){
    $rst=category_parent($data,$id);
    foreach (array_reverse($rst['pids']) as $v){
        foreach ($data as $vv){
            ($vv['parent_id']==$v) && $rst['parent'][$v][]=$vv;
        }
    }
    return $rst;
}
//根据分类id查找父类
function category_parent($data,$id=0){
    $rst=array('pact'=>array(),'pids'=>array($id));
    for ($i=0;$id && $i<10;++$i){//最多10层循环，防止意外死循环
        foreach ($data as $v){
            if($v['cate_id']==$id){
                $rst['pact'][]=$v;//父分类
                $rst['pids'][]=$id=$v['parent_id'];//父分类id
            }
        }
    }
    return $rst;
}

//通过开源组件HTML Purifier过滤富文本
//该函数用于在后台编辑商品详情时过滤富文本，过滤后保存到数据库
function htmlpurifier($html){
    static $purifier;
    if(empty($purifier)){
        //载入第三方类库
        if(!vendor('htmlpurifier.HTMLPurifier','','.standalone.php')){
            die('载入到HTMLPurifier 类库失败！');
        }
        $purifier=new HTMLPurifier($html);
    }
    return $purifier->purify($html);
}
function mkFilterURL($k,$v){
    $get=I('get.');
    unset($get['p']);
   if($k=='cid'){
       unset($get['price']);
   }
   
   $get[$k]=$v;
      
   return U('Index/find',$get);
    
}
















