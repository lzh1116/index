<?php
namespace Home\Controller;

class OrderController extends CommonController{
    public function __construct(){
        parent::__construct();
        if($this->userinfo===false){
            $this->error('请先登陆',U('User/login'));
        }
        $this->assign('title','我的订单 - 清新商城');
    }
    public function index(){//查看订单
        //取出订单列表
        $data=M('order')->where(array(
            'user_id'=>$this->userinfo['id'],
            'cancel'=>'no'))->order('id desc')->select();
        foreach ($data as $k=>$v){
            $data[$k]['goods']=unserialize($data[$k]['goods']);
            $data[$k]['address']=unserialize($data[$k]['address']);
        }
        //print_r($data);
        $this->assign('order',$data);
        $this->display();
    }
    public function buy(){
        //获取参数，cart=1表示多件商品
        if(I('get.cart')){
            $id=I('post.id/a');//商品id数组
            $num=I('post.num/a');//购买数量数组
        }else{
            $id=array(I('post.id'));
            $num=array(I('post.num'));
        }
        foreach ($id as $k=>$v){//过滤输入           
            if(!isset($num[$k])){//两个数组参数必须一致
                $this->error('执行失败，参数不正确');
            }
            $id[$k]=max((int)$id[$k],0);//商品id不能为负
            $num[$k]=max((int)$num[$k],1);//购买数量最少为1
        }
        $Goods=M('goods');
        $order=M('order');
        $this->checkToken($order);//检查表单令牌
        $uid=$this->userinfo['id'];//当前用户id
        $data=array(//准备待写入数据库的数据
            'price'=>0,//订单总价格
            'payment'=>'no',//订单未支付
            'cancel'=>'no',//订单未取消
            'user_id'=>$uid,//购买者的用户id
            'add_time'=>time(),
        );
        //获取收件人信息
        $data['address']=M('user')->field('consignee,address,phone')->where(array('id'=>$uid))->find();
        //没有收件人，不允许购买
        if(empty($data['address']['consignee']) || empty($data['address']['address']) || empty($data['address']['phone'])){
            $this->error('请完善收货信息',U('User/addr'));
        }
        //根据id取出商品名和价格
        $goods=$Goods->field('name,price')->where(array('id'=>array('in',$id)))->select();
        $order->startTrans();//开启事务
        foreach ($goods as $k=>$v){
            //组合商品信息
            $data['goods'][]=array(
                'id'=>$id[$k],//商品id
                'num'=>$num[$k],//购买数量
                'name'=>$v['name'],//商品名
                'price'=>$v['price'],//价格
            );
            //准备库存操作的where条件
            $where=array(
                'id'=>$id[$k],//商品id
                'stock'=>array('EGT',$num[$k]),//库存大于购买数量
                'recycle'=>'no',//
                'on_sale'=>'yes',
            );
            //更新库存
            if(!$Goods->where($where)->setDec('stock',$num[$k])){
                $order->rollback();//回滚
                $this->error('执行失败，商品"'.$v['name'].'"库存不足');
            }
            //价格自增
            $data['price'] += $v['price'] * $num[$k];
        }
        //数组序列化
        $data['address']=serialize($data['address']);
        $data['goods']=serialize($data['goods']);
        //保存订单
        if(!$order->data($data)->add()){
            $order->rollback();//回滚
            $this->error('执行失败，生成订单失败');
        }
        $order->commit();//提交事务
        //生成订单成功，删除购物车中的记录
        M('shopcart')->where(array(
            'goods_id'=>array('in',$id),
            'user_id'=>$uid))->delete();
        $this->success('订单生成成功',U('Order/index'));
    }
    public function cancel(){//取消订单
        $id=I('post.id/d',0);
        $Goods=M('goods');
        $order=M('order');
        $this->checkToken($order);//检查表单令牌
        //将订单中的商品返库存
        $data_order=$order->where(array('id'=>$id,'user_id'=>$this->userinfo['id']))->find();
        $data_goods=unserialize($data_order['goods']);
        foreach ($data_goods as $v){
            $Goods->where(array('id'=>$v['id']))->setInc('stock',$v['num']);
        }
        //删除订单(如果没有订单回收站功能，则执行此操作)
        $order->where(array('id'=>$id,'user_id'=>$this->userinfo['id']))->delete();
        $this->redirect('Order/index');
    }
    private function checkToken($Model){//检查表单令牌
        if(!$Model->autoCheckToken($_POST)){
            $this->error('表单已过期，请重新提交');
        }
    }
  
    
    
    
    
}









