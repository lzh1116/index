<?php
namespace Home\Controller;
//前台主页控制器
class IndexController extends CommonController {
    public function index(){
        //获取分页列表
        $data['category']=D('category')->getTree();
        
       //print_r( $data['category']);die();//获取无限级分类
        //准备查询条件(推荐商品，已上架，不在回收站)
        $where=array('recommend'=>'yes','on_sale'=>'yes','recycle'=>'no');
        //取出商品id，商品名，价格，图片
        $data['best']=M('goods')->field('id,name,price,thumb')->where($where)->limit(12)->select();
        //print_r($data['best']);die();
        $this->assign('title','清新商城首页');
        $this->assign($data);
        $this->display();
    }
    public function find(){//查找商品        
        //获取参数
        $p=I('get.p/d',0);//当前页码
        $cid=I('get.cid/d',-1);//分类id
        //实例化模型
        $goods=D('goods');
        $category=D('category');
        //如果分类id>0,则取出所有子分类id
        
        $cids=($cid>0)?$category->getSubIds($cid):$cid;
        //print_r($cids);die();
        //获取商品列表
        $data['goods']=$goods->getList($cids,$p);
        //防止空页被访问
        if(empty($data['goods']['data']) && $p>0){
            $this->redirect('Index/find',array('cid'=>$cid));
        }
        //查询分页列表
        $data['category']=$category->getFamily($cid);
        $data['cid']=$cid;
        $data['p']=$p;
        //print_r($data);die();
        $this->assign('title','商品列表   — 清新商城');
        $this->assign($data);
        $this->display();
    }
    public function goods(){//商品详情页
        $id=I('get.id/d',0);
        $goods=D('goods');
        $category=D('category');
        
        //查找当前商品
        $data['goods']=$goods->getGoods(array(
            'recycle'=>'no',
            'on_sale'=>'yes',
            'id'=>$id,
        ));
        
        if(empty($data['goods'])){
            $this->error('您访问的商品不存在，已下架或删除!');
        }
        //查找推荐商品
        $cids=$category->getSubIds($data['goods']['cate_id']);
        $where=array('recycle'=>'no','on_sale'=>'yes');
        $where['cate_id']=array('in',$cids);
        $data['recommend']=$goods->getRecommend($where);
        //查找分类导航
        $data['path']=$category->getPath($data['goods']['cate_id']);
        //print_r($data['path']);
        $this->assign('title',$data['goods']['name'].' - 清新商城');
        $this->assign($data);
        $this->display();
    }
    
    
}


