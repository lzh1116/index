<?php
namespace Home\Controller;


class UserController extends CommonController{
    public function __construct(){
        parent::__construct();
        $allow_action=array(
            //指定不需要检查登陆的方法列表
            'login','getVerify','checkVerify','register'
        );
        if($this->userinfo===false && !in_array(ACTION_NAME,$allow_action)){
            $this->error('请先登录',U('User/login'));
        }
        $this->assign('title','会员中心 - 清新商城');
    }
    //会员中心首页
    public function index(){
        $this->display();
    }
    //查看收件地址
    public function addr(){
        $data['addr']=D('user')->getAddr($this->userinfo['id']);
        $this->assign($data);
        $this->display();
    }
    //修改收货地址
    public function addrEdit(){
        if(IS_POST){
            $user=D('user');
            if(!$user->create(null,2)){//create()参数必须写
                $this->error('修改失败：'.$user->getError(),U('User/addrEdit'));
            }
            if(false===$user->where(array('id'=>$this->userinfo['id']))->save()){
                $this->error('修改失败：',U('User/addrEdit'));
            }
            $this->redirect('User/addr');
        }
        $this->addr();
    }
    public function login(){
        if(IS_POST){
            //检查验证码
            if(false===$this->checkVerify(I('post.verify'))){
                $this->error('验证码错误',U('User/login'));
            }
            $user=D('user');
            if(!$user->create()){
                $this->error('登陆失败：'.$user->getError(),U('User/login'));
            }
            //检查用户名密码
            if($userinfo = $user->checkLogin()){
                //登陆成功
                session('userinfo',$userinfo);//将登陆信息保存到session
                $this->redirect('Index/index');
            }
            $this->error('登录失败：用户名或密码错误',U('User/login'));
        }
        $this->display();
    }
    public function register(){
        if(IS_POST){
            //检查验证码
            if(false===$this->checkVerify(I('post.verify'))){
                $this->error('验证码错误',U('User/register'));//指定跳转地址，防止验证码不刷新
            }
            $user=D('user');
            //判断用户名是否存在
            if($user->where(array('username'=>I('post.username')))->getField('id')){
                $this->error('注册失败：用户名已经存在');
            }
            if(!$user->create(null,1)){
                $this->error('注册失败：'.$user->getError(),U('User/register'));
            }
            $username=$user->username;//取出用户名
            if(!$id=$user->add()){//添加数据，并取出新用户id
                $this->error('注册失败：保存到数据库失败',U('User/register'));
            }
            //注册成功后自动登录
            session('userinfo',array('id'=>$id,'name'=>$username));
            $this->redirect('Index/index');
        }
        $this->display();
    }
    public function getVerify(){//生成验证码
        $verify=new \Think\Verify();
        $verify->entry();
    }
    private function checkVerify($code,$id=''){//检查验证码
        $verify=new \Think\Verify();
        return $verify->check($code,$id);
    }
    public function logout(){
        session(null);
        $this->redirect('Index/index');
    }
    
    
    
    
    
    
}















