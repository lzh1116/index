<?php
namespace Home\Model;
use Think\Model;

class ShopcartModel extends Model{
    public function addCart($gid,$uid,$num){//添加购物车
        //判断购物车中是否已有该类商品
        $rst=$this->where(array('goods_id'=>$gid,'user_id'=>$uid))->field('id,goods_id,num')->find();
        if($rst){//存在商品时，增加购买数量
            $num += $rst['num'];
            return $this->where(array('id'=>$rst['id']))->save(array('num'=>$num));
        }
        //不存在时，添加到购物车
        return $this->add(array('user_id'=>$uid,'goods_id'=>$gid,'num'=>$num,'add_time'=>time()));
    }
    //从购物车中获取商品信息
    public function getList($uid){
        return $this->alias('c')->join('__GOODS__ as g on g.id=c.goods_id','left')
                                ->field('g.name,g.price,c.id,c.add_time,c.goods_id,c.num')
                                ->where(array('user_id'=>$uid))->select(); 
        
    }
    
    
    
    
}


