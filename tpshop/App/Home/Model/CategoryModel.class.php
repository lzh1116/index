<?php
namespace Home\Model;
use Think\Model;

class CategoryModel extends Model{
    private function getData(){//查询分类数据
        static $data=null;//缓存查询结果
        if(!$data){
          $data=$this->field('cate_id,cate_name,parent_id')->select();
         
        }
        return $data;
    }
    public function getTree($level=3){//获得分类列表
        return category_tree($this->getData(),0,$level);
    }
    public function getSubIds($id){//查找所有子孙分类id
        $data=array($id);//将自身id放入数组头部
        category_child($this->getData(),$data,$id);
        
        return $data;
    }
    public function getFamily($id){//查找分类家谱
        $id=max($id,0);
        return category_family($this->getData(),$id);
    }
    public function getPath($id){//查找分类面包屑导航
        $rst=category_parent($this->getData(),$id);
        
        return array_reverse($rst['pact']);
        
    }
    
    
    


}

