<?php
namespace Home\Model;
use Think\Model;

class UserModel extends Model{
    protected $insertFields='username,password';
    protected $updateFields='phone,email,consignee,address';
    protected $_validate=array(
        //注册时验证
        array('username','2,20','用户名位数不合法(2~20位)',1,'length',1),
        array('username','/^[\w\x{4e00}-\x{9fa5}]+$/u','用户名不符合要求',1,'regex',1),
        array('password','5,20','密码位数不合法(5~20位)',1,'length',1),
        array('password','/^\w+$/','密码只能是数字，字母，下划线',1,'regex',1),
        //更新时验证
        array('email','email','邮箱格式不正确',1,'regex',2),
        array('phone',11,'手机号码格式不正确',1,'length',2),
        array('consignee','require','收件人不能为空',1,'regex',2),
        array('address','require','收件地址不能为空',1,'regex',2),
    );
    //获取收件地址
    public function getAddr($id){
        //取出数据（收件人，收件地址，邮箱，手机号）
        $data=$this->field('consignee,address,email,phone')->where("id=$id")->find();
        //分割收件地址字符串
        $data['area']=explode(',', $data['address'],4);//最多分割4次
        if(count($data['area'])!=4){
            $data['area']=array('','--请选择--','--请选择--','');
        }
        return $data;
    }
    //判断用户名和密码
    public function checkLogin(){
        $username=$this->data['username'];//表单提交的用户名
        $password=$this->data['password'];//表单提交的密码
        //根据用户名查询密码
        $data=$this->field('id,password,salt')->where(array('username'=>$username))->find();
        //判断密码
        if($data && $data['password']==$this->password($password,$data['salt'])){
            return array('id'=>$data['id'],'name'=>$username);
        }
        return false;
    }
    //密码加密函数
    private function password($pwd,$salt){
        return md5(md5($pwd).$salt);
    }
    protected function _before_insert(&$data, $option){
        $data['salt']=substr(uniqid(),-6);
        $data['password']=$this->password($data['password'], $data['salt']);
    }
   

    
    
    
}



