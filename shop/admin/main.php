<?php
/**
 * @Author: Marte
 * @Date:   2017-07-14 09:51:21
 * @Last Modified by:   Marte
 * @Last Modified time: 2017-07-14 10:03:25
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<center>
    <h3>系统信息</h3>
    <table width="70%" border="1" cellpadding="5" cellspacing="0" bgcolor="#cccccc">
        <tr>
            <th>操作系统</th>
            <td>WINNT</td>
        </tr>
        <tr>
            <th>Apache版本</th>
            <td>Apache/2.4.18 (Win32) OpenSSL/1.0.2e PHP/5.5.30</td>
        </tr>
        <tr>
            <th>PHP版本</th>
            <td>5.5.30</td>
        </tr>
        <tr>
            <th>运行方式</th>
            <td>apache2handler</td>
        </tr>
    </table>
    <h3>软件信息</h3>
    <table width="70%" border="1" cellpadding="5" cellspacing="0" bgcolor="#cccccc">
        <tr>
            <th>系统名称</th>
            <td>清新网电子商城</td>
        </tr>
        <tr>
            <th>开发团队</th>
            <td>清新网的小伙伴</td>
        </tr>
        <tr>
            <th>公司网址</th>
            <td><a href="http://www.qingxin.com">http://www.qingxin.com</a></td>
        </tr>
        <tr>
            <th>成功案例</th>
            <td>清新网</td>
        </tr>
    </table>
</center>

</body>
</html>