<?php

require_once '../include/init.php';
require_once '../core/cate.php';
require_once '../core/pro.php';
require_once '../core/thumb.php';
$cates=getAllCate();

$sql='select * from pro where id='.$_GET['id'];

$row=fetchRow($sql);
// print_r($row);




?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>-.-</title>
<link href="../css/global.css"  rel="stylesheet"  type="text/css" media="all" />
<script type="text/javascript" charset="utf-8" src="../list/kindeditor.js"></script>
<script type="text/javascript" charset="utf-8" src="../list/zh_CN.js"></script>
<script type="text/javascript" src="../css/jquery-1.6.4.js"></script>
<script>
        KindEditor.ready(function(K) {
                window.editor = K.create('#editor_id');
        });
        $(document).ready(function(){
            $("#selectFileBtn").click(function(){
                $fileField = $('<input type="file" name="thumbs[]"/>');
                $fileField.hide();
                $("#attachList").append($fileField);
                $fileField.trigger("click");
                $fileField.change(function(){
                $path = $(this).val();
                $filename = $path.substring($path.lastIndexOf("\\")+1);
                $attachItem = $('<div class="attachItem"><div class="left">a.gif</div><div class="right"><a href="#" title="删除附件">删除</a></div></div>');
                $attachItem.find(".left").html($filename);
                $("#attachList").append($attachItem);
                });
            });
            $("#attachList>.attachItem").find('a').live('click',function(obj,i){
                $(this).parents('.attachItem').prev('input').remove();
                $(this).parents('.attachItem').remove();
            });
        });
</script>
</head>
<body>
<h3>添加商品</h3>
<form action="doAdminAction.php?act=editPro&id=<?php echo $_GET['id'];?>" method="post" enctype="multipart/form-data">
<table width="70%"  border="1" cellpadding="5" cellspacing="0" bgcolor="#cccccc">
    <tr>
        <td align="right">商品名称</td>
        <td><input type="text" name="name"  value="<?php echo $row['name']?>"/></td>
    </tr>
    <tr>
        <td align="right">商品分类</td>
        <td>
        <select name="cid">
  <?php foreach($cates as $c){
                        ?>
                            <option value="<?php echo $c['id'];?>" <?php if($row['cid']==$c['id']) echo 'selected';?>>
                                <?php echo $c['cate'];?>
                            </option>
    <?php };?> </select>
        </td>
    </tr>
    <tr>
        <td align="right">商品货号</td>
        <td><input type="text" name="pSn"  value="<?php echo $row['pSn']?>"/></td>
    </tr>
    <tr>
        <td align="right">商品数量</td>
        <td><input type="text" name="pNum"  value="<?php echo $row['pNum']?>"/></td>
    </tr>
    <tr>
        <td align="right">商品市场价</td>
        <td><input type="text" name="mPrice"  value="<?php echo $row['mPrice']?>"/></td>
    </tr>
    <tr>
        <td align="right">商品清新价</td>
        <td><input type="text" name="iPrice"  value="<?php echo $row['iPrice']?>"/></td>
    </tr>
    <tr>
        <td align="right">商品描述</td>
        <td>
            <textarea name="pDesc" id="editor_id" style="width:100%;height:150px;background-color:#FFFFFF;">
    <?php echo $row['pDesc'];?>
</textarea>
        </td>
    </tr>
    <tr>
        <td align="right">商品图像</td>
      <td>

            <a href="javascript:void(0)" id="selectFileBtn">添加附件</a><br/><br/>
 <?php
$rs=getImgNameByPid($row['id']);
foreach($rs as $r){

  ?>

<?php echo $r['name'];?>
            <div id="attachList" class="clear"></div><?php } ?>
        </td>
    </tr>
    <tr>
        <td colspan="2"><input type="submit"  value="编辑商品"/></td>
    </tr>
</table>
</form>
</body>
</html>