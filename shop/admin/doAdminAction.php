<?php
/**
 * @Author: Marte
 * @Date:   2017-07-11 09:52:25
 * @Last Modified by:   Marte
 * @Last Modified time: 2017-08-17 09:16:21
 */

require_once '../include/init.php';
require_once '../core/admin.php';
require_once '../core/user.php';
require_once '../core/cate.php';
require_once '../core/pro.php';
require_once '../core/thumb.php';



$act=isset($_GET['act'])?$_GET['act']:null;
$id=isset($_GET['id'])?$_GET['id']:null;
switch ($act) {
    case 'addAdmin':addAdmin();
        break;
    case 'editAdmin':echo editAdmin($id);
        break;
    case 'delAdmin':echo delAdmin($id);
        break;
    case 'addUser':addUser();
        break;
    case 'editUser':echo editUser($id);
        break;
    case 'delUser':echo delUser($id);
        break;
    case 'addCate':addCate();
        break;
    case 'editCate':echo editCate($id);
        break;
    case 'delCate':echo delCate($id);
        break;
    case 'addPro':addPro();
        break;
    case 'editPro':echo editPro($id);
        break;
    case 'delPro':echo delPro($id);
        break;
    case 'waterText':echo doWaterText($id);
        break;
    case 'waterPic':echo doWaterPic($id);
        break;
    default:
        # code...
        break;
}
