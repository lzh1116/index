<?php

require_once '../include/init.php';

require_once '../core/cate.php';

$page=isset($_GET['page'])?$_GET['page']:1;
$pageSize=2;

$list=getAllCatePage($page,$pageSize);
$totalpage=ceil(getTotalCatePage()/$pageSize);


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="../css/backstage.css">
</head>
<body>
<div class="details">
                    <div class="details_operation clearfix">
                        <div class="bui_select">
                            <input type="button" value="添&nbsp;&nbsp;加" class="add"  onclick="addCate()">
                        </div>

                    </div>
                    <!--表格-->
                    <table class="table" cellspacing="0" cellpadding="0">
                        <thead>
                            <tr>
                                <th width="15%">编号</th>
                                <th width="25%">分类</th>
                                <th>操作</th>
                            </tr>
                        </thead>
                        <tbody>
                          <?php foreach ($list as $k) {
                            # code...
                       ?>
                                                    <tr>
                                <!--这里的id和for里面的c1 需要循环出来-->
                                <td><input type="checkbox" id="c1" class="check"><label for="c1" class="label">
                                <?php echo $k['id'];?></label></td>
                                <td><?php echo $k['cate']?></td>
                                <td align="center"><input type="button" value="修改" class="btn" onclick="editCate(<?php echo $k['id']?>)"><input type="button" value="删除" class="btn"  onclick="delCate(<?php echo $k['id']?>)"></td>
                            </tr>
                    <?php };?>

 <tr>
 <td colspan="4">共<?php echo showpage($page,$totalpage);?></td>
                            </tr>
                                                    </tbody>
                    </table>
                </div>
<script type="text/javascript">
    function editCate(id){
        window.location="editCate.php?id="+id;
    }
    function delCate(id){
        if(window.confirm("您确定要删除吗？删除之后不能恢复哦！！！")){
            window.location="doAdminAction.php?act=delCate&id="+id;
        }
    }
    function addCate(){
        window.location="addCate.php";
    }
</script>
</body>
</html>