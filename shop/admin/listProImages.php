<?php

require_once '../include/init.php';
require_once '../core/pro.php';
require_once '../core/cate.php';
require_once '../core/thumb.php';


$sql="select pro.*,cate.cate from pro left join cate  on pro.cid=cate.id";

$list=fetchall($sql);



?>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>-.-</title>
<link rel="stylesheet" href="../css/backstage.css">
</head>

<body>

<div class="details">
                    <div class="details_operation clearfix">
                        <div class="bui_select">
                            <input type="button" value="添&nbsp;&nbsp;加" class="add" onclick="addPro()">
                        </div>
                        <div class="fr">
                            <div class="text">
                                <span>商品价格：</span>
                                <div class="bui_select">
                                    <select id="" class="select" onchange="change(this.value)">
                                        <option>-请选择-</option>
                                        <option value="iPrice asc" >由低到高</option>
                                        <option value="iPrice desc">由高到底</option>
                                    </select>
                                </div>
                            </div>
                            <div class="text">
                                <span>上架时间：</span>
                                <div class="bui_select">
                                 <select id="" class="select" onchange="change(this.value)">
                                    <option>-请选择-</option>
                                        <option value="pubTime desc" >最新发布</option>
                                        <option value="pubTime asc">历史发布</option>
                                    </select>
                                </div>
                            </div>
                            <div class="text">
                                <span>搜索</span>
                                <input type="text" value="" class="search"  id="search" onkeypress="search()" >
                            </div>
                        </div>
                    </div>
                    <!--表格-->
                    <table class="table" cellspacing="0" cellpadding="0">
                        <thead>
                            <tr>
                                <th width="10%">编号</th>
                                <th width="20%">商品名称</th>
                                <th>商品图片</th>
                                <th>操作</th>
                            </tr>
                        </thead>
                        <tbody>
                                                    <tr>
                                <!--这里的id和for里面的c1 需要循环出来-->
                                <?php foreach($list as $k){
                                    ?>
<td style="text-align: center;"><?php echo $k['id']?></td>
<td style="text-align: center;"><?php echo $k['name']?></td>
<td>
<?php
$rs=getImgNameByPid($k['id']);
foreach($rs as $r){

  ?>

<img width="100" height="100" src="../thumb_350/<?php echo $r['name'];?>" alt="" <?php echo $r['name'];?>/> &nbsp;&nbsp;

<?php };?>
</td>
                                 <td>
                                    <input type="button" value="添加文字水印" onclick="doImg(<?php echo $k['id']?>,'waterText')" class="btn"/>

                                    <br/>
                                        <input type="button" value="添加图片水印" onclick="doImg(<?php echo $k['id']?>,'waterPic')" class="btn"/>
                                 </td>

                            </tr><?php };?>
 </tbody>
                    </table>
                </div>
 <script type="text/javascript">
        function doImg(id,act){
            window.location="doAdminAction.php?act="+act+"&id="+id;
        }
 </script>
</body>
</html>



