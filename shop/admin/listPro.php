<?php

require_once '../include/init.php';
require_once '../core/pro.php';
require_once '../core/cate.php';
require_once '../core/thumb.php';



$page=isset($_GET['page'])?$_GET['page']:1;
$pageSize=2;

$list=getAllProPage($page,$pageSize);
$totalpage=ceil(getTotalProPage()/$pageSize);

$order=isset($_GET['order'])?$_GET['order']:null;
$keywords=isset($_GET['keywords'])?$_GET['keywords']:null;

$porder=$order?'&order='.$order:null;

$pkeywords=$keywords?'&keywords='.$keywords:null;

?>
<html>
<head>
<meta charset="utf-8">
<title>listPro.php</title>
<link rel="stylesheet" href="../css/backstage.css">
<link rel="stylesheet" href="../css/jquery-ui-1.10.4.custom.css" />
<script src="../list/jquery-1.10.2.js"></script>
<script src="../list/jquery-ui-1.10.4.custom.js"></script>
<script src="../list/jquery-ui-1.10.4.custom.min.js"></script>
</head>

<body>
<div id="showDetail"  style="display:none;">

</div>
<div class="details">
                    <div class="details_operation clearfix">
                        <div class="bui_select">
                            <input type="button" value="添&nbsp;&nbsp;加" class="add" onclick="addPro()">
                        </div>
                        <div class="fr">
                            <div class="text">
                                <span>商品价格：</span>
                                <div class="bui_select">
                                    <select id="" class="select" onchange="change(this.value)">
                                        <option>-请选择-</option>
                                        <option value="iPrice asc" >由低到高</option>
                                        <option value="iPrice desc">由高到底</option>
                                    </select>
                                </div>
                            </div>
                            <div class="text">
                                <span>上架时间：</span>
                                <div class="bui_select">
                                 <select id="" class="select" onchange="change(this.value)">
                                    <option>-请选择-</option>

                                        <option value="pubTime desc">最新发布</option>
                                        <option value="pubTime asc">历史发布</option>
                                    </select>
                                </div>
                            </div>
  <div class="text">
                                <span>搜索</span>
                                <input type="text" value="" class="search"  id="search" onkeypress="search(this.value)" >

                            </div>
                        </div>
                    </div>
                    <!--表格-->
                    <table class="table" cellspacing="0" cellpadding="0">
                        <thead>
                            <tr>
                                <th width="10%">编号</th>
                                <th width="20%">商品名称</th>
                                <th width="10%">商品分类</th>
                                <th width="10%">是否上架</th>
                                <th width="15%">上架时间</th>
                                <th width="10%">清新价格</th>
                                <th>操作</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php foreach($list as $k){

                            ?>
                                                    <tr>
                                <!--这里的id和for里面的c1 需要循环出来-->
                                <td><input type="checkbox" id="c25" class="check" value=25><label for="c1" class="label">
                <?php echo $k['id'];?></label></td>
                <td><?php echo $k['name'];?></td>
                <td><?php echo $k['cate'];?></td>
                <td> 上架 </td>
                <td><?php echo date('m-d H:i:s',$k['pubTime']);?></td>
                <td><?php echo $k['iPrice'];?>元</td>
                <td align="center">
<input type="button" value="详情" class="btn" onclick="showDetail(<?php echo $k['id'];?>,'111')">
<input type="button" value="修改" class="btn" onclick="editPro(<?php echo $k['id'];?>)">
<input type="button" value="删除" class="btn"onclick="delPro(<?php echo $k['id']?>)">
<div id="showDetail<?php echo $k['id'];?>" style="display:none;">
<table class="table" cellspacing="0" cellpadding="0">
<tr>
<td width="20%" align="right">商品名称</td>
 <td><?php echo $k['name']?></td>
                                    </tr>
                                        <tr>
  <td width="20%"  align="right">商品类别</td>
   <td><?php echo $k['cate']?></td>
                                    </tr>
                <tr>
 <td width="20%"  align="right">商品货号</td>
        <td><?php echo $k['pSn']?></td>
                 </tr>
<tr>
      <td width="20%"  align="right">商品数量</td>
         <td><?php echo $k['pNum']?></td>
          </tr>
        <tr>
     <td  width="20%"  align="right">商品价格</td>
     <td><?php echo $k['mPrice']?>元</td>
        </tr>
  <tr>
    <td  width="20%"  align="right">清新网价格</td>
    <td><?php echo $k['iPrice']?>元</td>
</tr>
<tr>
<td width="20%"  align="right">商品图片</td>
 <td>
<?php
$rs=getImgNameByPid($k['id']);
foreach($rs as $r){

  ?>


<img width="100" height="100" src="../thumb_800/<?php echo $r['name'];?>" alt="<?php echo $r['name'];?>" /> &nbsp;&nbsp;


<?php };?>
</td></tr>
<tr>
<td width="20%"  align="right">是否上架</td>
<td>上架</td>
</tr>
 <tr>
 <td width="20%"  align="right">是否热卖</td>
 <td>正常 </td>
     </tr>
</table>
<span style="display:block;width:80%; ">
商品描述<br/>
 <?php echo $k['pDesc'];?>  </span>
</div>
</td>
</tr>


<?php };?>
 <tr>
 <td colspan="7"><?php echo showpage($page,$totalpage,$porder.$pkeywords);?>
 </td></tr>
  </tbody>
  </table>
  </div>




<script type="text/javascript">
function showDetail(id,t){
    $("#showDetail"+id).dialog({
          height:"auto",
          width: "auto",
          position: {my: "center", at: "center",  collision:"fit"},
          modal:false,//是否模式对话框
          draggable:true,//是否允许拖拽
          resizable:true,//是否允许拖动
          title:"商品名称："+t,//对话框标题
          show:"slide",
          hide:"explode"
    });
}
    function addPro(){
        window.location='addPro.php';
    }
    function editPro(id){
        window.location='editPro.php?id='+id;
    }
    function delPro(id){
        if(window.confirm("您确认要删除嘛？添加一次不易，且删且珍惜!")){
            window.location="doAdminAction.php?act=delPro&id="+id;
        }
    }
    function search(){
        if(event.keyCode==13){
            var val=document.getElementById("search").value;
            window.location="listPro.php?keywords="+val;
        }
    }
    function change(val){
        window.location="listPro.php?order="+val;
    }
</script>
</body>
</html>


