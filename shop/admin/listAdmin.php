<?php
require_once '../include/init.php';

require_once '../core/admin.php';

$page=isset($_GET['page'])?$_GET['page']:1;
$pageSize=2;

$list=getAllAdminPage($page,$pageSize);
$totalpage=ceil(getTotalPage()/$pageSize);
// print_r($list);
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>listAdmin</title>
<link rel="stylesheet" href="../css/backstage.css">
</head>

<body>
<div class="details">
                    <div class="details_operation clearfix">
                        <div class="bui_select">
                            <input type="button" value="添&nbsp;&nbsp;加" class="add"  onclick="addAdmin()">
                        </div>

                    </div>
                    <!--表格-->
                    <table class="table" cellspacing="0" cellpadding="0">
                        <thead>
                            <tr>
                                <th width="15%">编号</th>
                                <th width="25%">管理员名称</th>
                                <th width="30%">管理员邮箱</th>
                                <th>操作</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($list as $k) {
                            # code...
                       ?>
                                                    <tr>
                                <!--这里的id和for里面的c1 需要循环出来-->
                                <td><input type="checkbox" id="c1" class="check"><label for="c1" class="label"><?php echo $k['id']?></label></td>
                                <td><?php echo $k['username']?></td>
                                <td><?php echo $k['email']?></td>
                                <td align="center"><input name="id"type="button" value="修改" class="btn" onclick="editAdmin(<?php echo $k['id']?>)"><input type="button" value="删除" class="btn"  onclick="delAdmin(<?php echo $k['id']?>)"></td>
                            </tr>

                            <?php }
                            ?>
  <tr>
 <td colspan="4"><?php echo showpage($page,$totalpage);?></td>
                            </tr>
                                                    </tbody>
                    </table>
                </div>
</body>
<script type="text/javascript">

    function addAdmin(){
        window.location="addAdmin.php";
    }
    function editAdmin(id){
            window.location="editAdmin.php?id="+id;
    }
    function delAdmin(id){
            if(window.confirm("您确定要删除吗？删除之后不可以恢复哦！！！")){
                window.location="doAdminAction.php?act=delAdmin&id="+id;
            }
    }
</script>
</html>