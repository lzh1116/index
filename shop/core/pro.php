<?php
/**
 * @Author: Marte
 * @Date:   2017-07-14 10:31:04
 * @Last Modified by:   Marte
 * @Last Modified time: 2017-08-21 17:43:27
 */

function addPro(){

    $arr=$_POST;
    $arr['pubTime']=time();
    $path="./uploads/";
    $uploadfiles=uploadfile($path);
//先添加商品 pid $img['pid'] $img['name']
    if(is_array($uploadfiles)&&$uploadfiles){
        foreach($uploadfiles as $k=>$uploadfile){
            thumb($path."/".$uploadfile['name'],"../thumb_50/".$uploadfile['name'],50,50);
            thumb($path."/".$uploadfile['name'],"../thumb_350/".$uploadfile['name'],350,350);
            thumb($path."/".$uploadfile['name'],"../thumb_800/".$uploadfile['name'],800,800);
        }
    }
    $res=insert("pro",$arr);
    $pid=getInsertId();
    //echo $pid,'----',$res;
    if($res&&$pid){
        foreach($uploadfiles as $uploadfile){
            $arr1['pid']=$pid;
            $arr1['name']=$uploadfile['name'];
            addAlbum($arr1);
        }
        $mes="添加成功!<br/><a href='addPro.php' target='mainFrame'>继续添加</a>
        &nbsp;&nbsp;&nbsp;&nbsp;<a href='listPro.php' target='mainFrame'>查看商品列表</a>";
    }else{
         $mes="编辑失败!<br/><a href='editPro.php'>请重新修改</a>";
    }
    return $mes;
}

function editPro($id){
    $arr=$_POST;

    if(update("pro",$arr,"id={$id}")){
        $mes="编辑成功！<br/><a href='listPro.php'>查看商品列表</a>";
    }else{
        $mes="编辑失败!<br/><a href='listPro.php'>请重新修改</a>";
    }
    return $mes;
}

function delPro($id){

    if(delete("pro","id={$id}")){
        $mes="删除成功！<br/><a href='listPro.php'>查看商品列表</a>";
    }else{
        $mes="删除失败！<br/><a href='listPro.php'>请重新删除</a>";
    }
    return $mes;
}


// function getUserById(){


// }

function getAllProPage($page,$pageSize){
    $order=isset($_GET['order'])?$_GET['order']:1;
    $keywords=isset($_GET['keywords'])?$_GET['keywords']:'';
    $where=" where name like '%$keywords%'";
    $offset=($page-1)*$pageSize;

    $sql="select pro.*,cate.cate from pro left join cate on pro.cid=cate.id $where order by $order limit $offset,$pageSize";
    return fetchall($sql);
}

function getTotalProPage(){
  return   getresnum('select * from pro');

}

function addAlbum($arr){
   return insert("thumb",$arr);
}

function getGood4($cid){
    $sql="select * from pro where cid=$cid limit 0,4";
    return fetchall($sql);
}

function getGood8($cid){
   $sql="select * from pro where cid=$cid limit 4,4";
    return fetchall($sql);
}
