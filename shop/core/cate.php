<?php
/**
 * @Author: Marte
 * @Date:   2017-07-14 10:30:54
 * @Last Modified by:   Marte
 * @Last Modified time: 2017-08-17 09:13:09
 */

function addCate(){
     $arr=$_POST;
    if(insert('cate',$arr)){
        alertMsg('添加分类成功','listCate.php');
    }else{
        echo '添加失败';
    }

}

function editCate($id){
    $arr=$_POST;

    if(update("cate",$arr,"id={$id}")){
        $mes="编辑成功！<br/><a href='listCate.php'>查看分类列表</a>";
    }else{
        $mes="编辑失败!<br/><a href='listCate.php'>请重新修改</a>";
    }
    return $mes;
}

function delCate($id){
    if(delete("cate","id={$id}")){
        $mes="删除成功！<br/><a href='listCate.php'>查看分类列表</a>";
    }else{
        $mes="删除失败！<br/><a href='listCate.php'>请重新删除</a>";
    }
    return $mes;
}

function getAllCatePage($page,$pageSize){
    $offset=($page-1)*$pageSize;
    return fetchall("select * from cate limit $offset,$pageSize");
}
function getTotalCatePage(){
  return   getresnum('select * from cate');

}

function getAllCate(){

    return fetchall("select * from cate");
}

