<?php
/**
 * @Author: Marte
 * @Date:   2017-07-10 10:20:57
 * @Last Modified by:   Marte
 * @Last Modified time: 2017-07-14 10:28:32
 */
//初始化文件
//
//
define('ROOT',str_replace("\\",'/',dirname(dirname(__FILE__))).'/');

require_once ROOT.'include/mysql.php';
require_once ROOT.'include/string.php';
require_once ROOT.'include/img.php';
require_once ROOT.'include/upload.php';
require_once ROOT.'include/page.php';
require_once ROOT.'include/common.php';

session_start();
connect();
// conn 开启 session