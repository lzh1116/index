<?php
/**
 * @Author: Marte
 * @Date:   2017-07-10 10:17:40
 * @Last Modified by:   Marte
 * @Last Modified time: 2017-08-25 09:28:18
 */
//
//上传图片函数
//

function buildinfo(){
    if(!$_FILES){
        return ;
    }
    $i=0;
     $files=array();
    foreach($_FILES as $v){
        //单文件
        if(is_string($v['name'])){
            $files[$i]=$v;

        }else{
            //多文件
            foreach($v['name'] as $k=>$vv){
                $files[$k]['name']=$vv;
                $files[$k]['size']=$v['size'][$k];
                $files[$k]['tmp_name']=$v['tmp_name'][$k];
                $files[$k]['error']=$v['error'][$k];
                $files[$k]['type']=$v['type'][$k];

            }
        }
    }
    return $files;
}
// var_dump(buildinfo());
//
//----
function uploadfile($path="uploads",$allowext=array("gif","jpeg","png","jpg"),
    $maxsize=2097152,$imgflag=true){
    if(!file_exists($path)){
        mkdir($path,0777,true);
    }
    $i=0;
    $files=buildinfo();
    if(!($files && is_array($files))){
        return ;
    }
    foreach($files as $file){
        if($file['error']===UPLOAD_ERR_OK){
            $ext=getext($file['name']);
            //检测文件的扩展名;
            if(!in_array($ext,$allowext)){
                exit('非法文件类型');
            }
            //校验是否是一个真正的图片类型;
            if($imgflag){
                if(!getimagesize($file['tmp_name'])){
                    exit('不是真正的图片类型');
                }
            }
            //上传文件的大小
            if($file['size'] > $maxsize){
                exit('上传的文件过大');
            }
            if(!is_uploaded_file($file['tmp_name'])){
                exit('不是通过HTTP POST方式上传的');
            }
            $filename=getuniname().".".$ext;
            $destination=$path."/".$filename;

            if(move_uploaded_file($file['tmp_name'],$destination)){
                $file['name']=$filename;
                unset($file['tmp_name'],$file['size'],$file['type']);
                $uploadedfiles[$i]=$file;
                $i++;
            }
        }else{
            switch ($file['error']) {
                case 1:
                    $mes="超过了配置文件上传文件的大小";
                    //UPLOAD_ERR_INI_SIZE;
                    break;
                case 2:
                    $mes="超过了表单设置上传文件的大小";
                    //UPLOAD_ERR_FORM_SIZE;
                    break;
                case 3:
                    $mes="文件部分被上传";
                    //UPLOAD_ERR_PARTIAL;
                    break;
                case 4:
                    $mes="没有文件被上传";
                    //UPLOAD_ERR_NO_FILE;
                    break;
                case 6:
                    $mes="没有找到临时目录";
                    //UPLOAD_ERR_NO_TMP_DIR;
                    break;
                case 7:
                    $mes="文件不可写";
                    //UPLOAD_ERR_CANT_WRITE;
                    break;
                case 8:
                    $mes="由于php的扩展程序中断了文件上传";
                    //UPLOAD_ERR_EXTENSION;
                    break;
            }
            echo $mes;
        }
    }
    return $uploadedfiles;
}

