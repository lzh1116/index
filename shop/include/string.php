<?php
/**
 * @Author: Marte
 * @Date:   2017-07-10 10:16:41
 * @Last Modified by:   Marte
 * @Last Modified time: 2017-08-15 10:56:03
 */
// 随机字符 唯一的 获取后缀 strrchr


//
//字符串函数；
//↓↓↓↓↓↓↓↓

function buildrandomstring($type=1,$length=4){
    if($type==1){
        $chars=join("",range(0,9));
    }elseif($type==2){
        $chars=join("",array_merge(range("a","z"),
            range("A","Z")));
    }elseif($type==3){
        $chars=join("",array_merge(range("a","z"),
            range("A","Z"),range(0,9)));
    }

    if($length>strlen($chars)){
        exit('字符串长度不够');
    }

    $chars=str_shuffle($chars);
    return substr($chars,0,$length);

}



function getuniname(){
    return md5(uniqid(microtime(true),true));//获得唯一字符串
}


function getext($filename){
   return strtolower(substr(strrchr($filename,'.'),1));
    //return strtolower(end(explode(".",$filename)));//获取文件名后缀转小写
}



