<?php

require'mysql.php';
$list=fetchall('select thread.*,(select MAX(reptime) from reply where thread.tid=reply.tid) as mtime,(select username from reply where thread.tid=reply.tid ORDER BY reptime desc limit 1) as ruser,(select COUNT(*) from reply where thread.tid=reply.tid) as rnum from thread ORDER BY if(mtime>pubtime,mtime,pubtime) desc');

// print_r($list);


?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN""
"http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="zh-cn">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <title>php吧</title>
    <link rel="stylesheet" href="./css/index.css" type="text/css" media="screen" charset="utf-8">
    <link rel="stylesheet" href="./css/reset.css" type="text/css" media="screen" charset="utf-8">
</head>
<body>
<div class="head">
   <a href="index.php">php吧</a>
</div>
<div class="container">
<div class="contentlist">
    <ul class="threadlist">
    <?php foreach ($list as $k){

    ?>
<!--一个贴子开始处-->
        <li class="threadlist_li">
            <div class="threadlist_left">
                <div class="threadlist_rep_num" title="回复数">
                <?php echo $k['rnum'];?></div>
            </div>
            <div class="threadlist_right">
                <div class="threadlist_lz">
                    <div class="threadlist_title">
                        <a href="tie.php?tid=<?php echo $k['tid'];?>" title="帖子标题" target="_blank" class="j_th_tit">
                        <?php echo $k['title'];?></a>
                    </div>
                    <div class="threadlist_author" title="帖子作者">
                        <span class="tb_icon_author"><?php echo $k['username'];?></span>
                    </div>
                </div>
                <div class="threadlist_detail">
                    <div class="threadlist_text">
                    <div class="threadlist_abs"><?php echo $k['content'];?></div>
                    </div>
                    <div class="threadlist_author">
                    <span class="tb_icon_author_rely" title="最后回复人">
                    <?php echo $k['ruser'];?></span>
                    <span class="threadlist_reply_date" title="最后回复时间">
                    <?php echo date('Y/m/d  H:i:s',$k['mtime']);?></span>
                    </div>
                </div>
            </div>
            <div class="del">
                <form action="del.php" method="post">
                    <p><input type="hidden" name="tid" value="<?php echo $k['tid'];?>"></p>
                    <p><input type="submit" value="删除" style="position:absolute;right:300px;"></p>
                </form>
            </div>
<div class="clear"></div>
        </li>
        <?php } ?>
<!--一个帖子结束处-->

    </ul>
</div>




</div>
<div class="editor">
    <form action="pubaction.php" method="post">
        <div id="editor_title_txt">
            发表新帖
        </div>
        <p>用户名:<input type="text" name="username" class="edit_title_field" value=""></p>
        <p>标题名:<input type="text" name="title" class="edit_title_field" value=""></p>

        <p><textarea name="content" rows="8" cols="40"></textarea></p>
        <p><input type="submit" class="subbtn_bg" value="发表"></p>
    </form>
</div>
</body>
</html>
