<?php

class OIModel extends model{
    protected $table='orderinfo';
    protected $pk='order_id';
    protected $fields=array('order_id','order_sn','user_id','username','reciver','address','mobile','add_time','order_amount','pay');
    protected $_valid=array(
        array('reciver',1,'收货人不能为空','require'),
        array('pay',0,'必须选择支付方式','in','1,2'));
    protected $_auto=array(
        array('add_time','function','time'));
    public function orderSn(){
        $sn='OI'.date('Ymd') . mt_rand(10000,99999);
        $sql='select count(*) from orderinfo where order_sn='."'$sn'";
        return $this->db->getOne($sql)?$this->orderSn():$sn;
    }
    public function invoke($order_id){
        $this->delete($order_id);
        $sql='delete from ordergoods where order_id='.$order_id;
        return $this->db->query($sql);
    }
    public function getSn($order_id){
        $sql="select ordergoods.goods_name,ordergoods.subtotal from ordergoods where order_id=".$order_id;
        return $this->db->getAll($sql);
    }
    public function getSnInfo(){
        $sql="select orderinfo.*,(select count(*) from ordergoods where ordergoods.order_id=orderinfo.order_id) as num from orderinfo";
        return $this->db->getAll($sql);
    }
}






//$_POST['pubtime']=time() 10
//
////timestamp 自动填充当前时间 2017-01-09 14：30:30
// 添加的时间 修改的时间 date_time
// int timestamp datetime