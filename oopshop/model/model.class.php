<?php


class model{
    protected $table;
    protected $db;
    protected $fields=array();
    protected $_atuo=array();
    public function _facade(&$post){//自动过滤

        foreach ($post as $k => $v) {
            if(!in_array($k,$this->fields)){
                unset($post[$k]);

            }
        }

    }
    // 自动完成
    public function _autoFill($data){
        foreach ($this->_auto as $k => $v) {
            if(!array_key_exists($v[0],$data)){
                switch ($v[1]) {
                    case 'value':
                        $data[$v[0]]=$v[2];
                        break;
                    case 'function':
                        $data[$v[0]]=call_user_func($v[2]);
                        break;

                    default:
                        # code...
                        break;
                }
            }
        }
        return $data;
    }
    public function _validate($data){
        if(empty($this->_valid)){
            return true;
        }
        $this->error=array();
        foreach ($this->_valid as $k=> $v) {
            switch ($v[1]) {
                case 1:
                    if(!isset($data[$v[0]])){
                        $this->error[]=$v[2];
                        return false;
                    }
                    if(!$this->check($data[$v[0]],$v[3])){
                        $this->error[]=$v[2];
                        return false;
                    }
                    break;
                case 0:
                    if(isset($data[$v[0]])){
                        if(!$this->check($data[$v[0]],$v[3],$v[4])){
                            $this->error[]=$v[2];
                            return false;
                        }
                    }
                    break;
                case 2:
                    if(isset($data[$v[0]]) && !empty($data[$v[0]])){
                        if(!$this->check($data[$v[0]],$v[3],$v[4])){
                            $this->error[]=$v[2];
                            return false;
                        }
                    }

            }
        }
        return true;
    }

    public function getErr(){
        return $this->error;
    }
    protected function check($value,$rule='',$parm=''){
        switch ($rule) {
            case 'require':
                return !empty($value);

            case 'number':
                return is_numeric($value);

            case 'in':
                $tmp=explode(',',$parm);
                return in_array($value,$tmp);
            case 'between':
                list($min,$max)=explode(',',$parm);
                return $value >= $min && $value <= $max;

            case 'length':
                list($min,$max)=explode(',',$parm);
                return strlen($value) >= $min && strlen($value) <= $max;

            default:
                return false;
        }
    }



    public function __construct(){

        $this->db=mysql::getIns();
    }
    public function select(){
        return $this->db->getAll('select * from '.$this->table);

    }

    public function add($post){
         $this->db->autoExecute($this->table,$post);

         return $this->db->insert_id();

    }
    public function del($id){
        $sql='delete from ' . $this->table.' where id='.$id;
        $this->db->query($sql);
        return $this->db->affected_rows();
    }
    public function edit($data,$id){
        $sql=$this->db->autoExecute($this->table,$data,'update',' where id='.$id);
        return $this->db->affected_rows();
    }
    public function find($id){
        return $this->db->getRow('select * from ' . $this->table .' where id='.$id);
    }


}

