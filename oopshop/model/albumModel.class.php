<?php
/**
 * @Author: Marte
 * @Date:   2017-09-05 08:38:44
 * @Last Modified by:   Marte
 * @Last Modified time: 2017-09-06 16:34:11
 */
class albumModel extends model{
    protected $table='thumb';

    public function addAlbum($arr){
        return $this->add("thumb",$arr);
    }
}