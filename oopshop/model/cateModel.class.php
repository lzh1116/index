<?php

class cateModel extends model{

    protected $table='cate';
    protected $filds=array('cate_name','parent_id','intro');


    public function getTree(){
        $cates=$this->select();
        $trees=array();

        $this->tree($cates,$trees);
        return  $trees;
    }

    private function tree($arr,&$sub,$id=0,$lev=1){
            foreach ($arr as $k) {
                if($k['parent_id']==$id){
                    $k['lev']=$lev;
                    $sub[]=$k;
                    $this->tree($arr,$sub,$k['id'],$lev+1);
                }
            }
        }
    public function getSon($id){
        $ids=array();
        $this->getSonId($id,$ids);
        $ids=implode(',',$ids);

        $sql='select * from goods where cat_id in'.'('.$ids.')';
        return $this->db->getAll($sql);
    }

    public function getDcate(){
        return $this->db->getAll('select * from '.$this->table.' where parent_id=0');
    }
    public function getAllCate(){
        return $this->db->getAll('select * from '.$this->table);
    }
    public function getSonId($id,&$ids){
        $cates=$this->select();

        foreach ($cates as $v) {
            if($v['parent_id']==$id){
                $ids[]=$v['id'];

                $this->getSonId($v['id'],$ids);
            }
        }
    }


}
