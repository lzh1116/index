<?php
require '../include/init.php';
$goods=new goodsModel();
$pag=new page();
$album=new albumModel();
$pagesize=2;

//$page=$_REQUEST['page']?(int)$_REQUEST['page']:1;
$page=isset($_GET['page'])?$_GET['page']:1;
$order=isset($_GET['order'])?$_GET['order']:null;
$orderBy=$order?" order by ".$order:null;
$keywords=isset($_GET['keywords'])?$_GET['keywords']:null;
$where=$keywords?" and goods_name like '%{$keywords}%'":null;


$totalRows=$goods->getResultNum($where);
$totalPage=ceil($totalRows/$pagesize);
if($page<1||$page==null||!is_numeric($page))$page=1;

if($page>$totalPage)$page=$totalPage;
$offset=($page-1)*$pagesize;

$Order=$order?'&order='.$order:null;
$Keywords=$keywords?'&keywords='.$keywords:null;


$goodsPage=$pag->showpage($page,$totalPage,$Order.$Keywords);
$list=$goods->getGoods($offset,$pagesize,$orderBy,$where);

// $img=$goods->getImgNameByPid('30');


include('../view/admin/goodslist.html');