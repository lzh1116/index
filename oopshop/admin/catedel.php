<?php
/**
 * @Author: Marte
 * @Date:   2017-08-31 15:48:48
 * @Last Modified by:   Marte
 * @Last Modified time: 2017-09-01 15:04:07
 */

header('content-type:text/html;charset:utf-8');
require_once'../include/init.php';
$id=$_GET['id'];
$cate=new cateModel();
$son=$cate->getSon($id);
//print_r($son);die();
if(!empty($son)){
    exit('有子栏目,不允许删除');
}

if($cate->del($id)){
    echo '删除成功';
}else{
    echo '删除失败';
}