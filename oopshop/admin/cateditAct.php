<?php

require '../include/init.php';

$data=array();
if(empty($_POST['cate_name'])){
    exit('栏目名不能为空');
}
$data['cate_name']=$_POST['cate_name'];
$data['parent_id']=$_POST['parent_id'];
$data['intro']=$_POST['intro'];
$id=$_POST['id'];
$cat=new CateModel();

$trees=$cat->getTree($data['parent_id']);
$flag=true;
// print_r($trees);
// die();
foreach ($trees as $v) {
    if($v['id']==$id){
        $flag=false;
        break;
    }
}
if(!$flag){
    echo '父栏目选取错误';
    exit;
}

if($cat->edit($data,$id)){
    echo '修改成功';
}else{
    echo '修改失败';
}



