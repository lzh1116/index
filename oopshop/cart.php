<?php
require 'include/init.php';
$user=new userModel();
$u=$user->checkLogin();


if($check=$user->checkLogin()){
    //设置一个动作参数，判断->下单/写地址/提交/清空购物车等
    $act=isset($_GET['act'])?$_GET['act']:'';
    $cart=cart::getCart();//获取购物车实例
    $goods=new goodsModel();
    if($act=='buy'){


        //把商品添加到购物车中
        $goods_id=isset($_GET['goods_id'])?$_GET['goods_id']+0:0;
        $num=isset($_GET['num'])?$_GET['num']+0:1;
        if($goods_id){
            //$goods_id为真，把商品放到购物车中
            $g=$goods->find($goods_id);

            if(!empty($g)){//有此商品
                //判断此商品是否在回收站或是否已下架
                if($g['is_delete']==1 || $g['is_on_sale']==0){
                    common::alertMes('此商品不能购买，已缺货或已下架','goods.php?id='.$goods_id);
                    exit;
                }
                //先把商品加到购物车
                $cart->addItem($goods_id,$g['goods_name'],$g['sPrice'],$num);
                //判断库存够不够
                $items=$cart->all();
                if($items[$goods_id]['num'] > $g['goods_number']){
                    //库存不足，删除刚才添加的商品
                    $cart->decNum($goods_id,$num);
                    common::alertMes('库存不足，添加购物车失败','goods.php?id='.$goods_id);
                    exit;
                }else{
                    common::alertMes('添加购物车成功','goods.php?id='.$goods_id);
                }
            }
        }


    }else if($act=='tijiao'){
      $goods_id=isset($_GET['goods_id'])?$_GET['goods_id']+0:0;
        $num=isset($_GET['num'])?$_GET['num']+0:1;
        if($goods_id){
            //$goods_id为真，把商品放到购物车中
            $g=$goods->find($goods_id);

            if(!empty($g)){//有此商品
                //判断此商品是否在回收站或是否已下架
                if($g['is_delete']==1 || $g['is_on_sale']==0){
                    common::alertMes('此商品不能购买，已缺货或已下架','goods.php?id='.$goods_id);
                    exit;
                }
                //先把商品加到购物车
                $cart->addItem($goods_id,$g['goods_name'],$g['sPrice'],$num);
                //判断库存够不够
                $items=$cart->all();
                if($items[$goods_id]['num'] > $g['goods_number']){
                    //库存不足，删除刚才添加的商品
                    $cart->decNum($goods_id,$num);
                    common::alertMes('库存不足，购买失败','goods.php?id='.$goods_id);
                    exit;
                }else{
                  common::alertMes("购买成功,请确认订单",'cart.php?act=tijiao');
                }
            }
        }
        $items=$cart->all();

        $total=$cart->getPrice();//获取购物车中的商品总价格
        include(ROOT.'view/home/tijiao.html');


    }else if($act=='done'){
       $items=$cart->all();

       if(empty($items)){
        //若购物车为空，返回首页
        header('location:index.php');
        exit;
       }
       $OI=new OIModel();
       $data=$_POST;
       $data['add_time']=time();
       $total=$data['order_amount']=$cart->getPrice();
       $data['user_id']=isset($_SESSION['id'])?$_SESSION['id']:0;
       $data['username']=isset($_SESSION['username'])?$_SESSION['username']:'匿名';
       $order_sn=$data['order_sn']=$OI->orderSn();
       if(!$OI->_validate($data)){
        $msg=implode(',',$OI->getErr());
        exit($msg);
       }
       $order_id=$OI->add($data);
       //$order_id=$OI->insert_id();
       $items=$cart->all();
       $cnt=0;
       $OG=new OGModel();

       foreach ($items as $k=>$v){
           $data=array();
           //$data['order_sn']=$order_sn;
           $data['order_id']=$order_id;
           $data['goods_id']=$k;
           $data['goods_name']=$v['name'];
           $data['goods_number']=$v['num'];
           $data['sPrice']=$v['price'];
           $data['subtotal']=$v['price']*$v['num'];
           if($OG->addOG($data)){
            $cnt += 1;
           }
       }
       if(count($items)!==$cnt){
        //购物车的商品数量，并没有全部入库成功，撤销此订单
        $OI->invoke($order_id);
        $msg='下订单失败';
        exit($msg);
       }else{
        common::alertMes('购买成功',ROOT.'/view/home/order.html');
       }
       $cart->clear();
       include(ROOT.'view/home/cart.html');
    }else{

      $items=$cart->all();
      //print_r($items);die();
        if(empty($items)){
            //若购物车为空，则返回首页
            header('location:index.php');
            exit;
        }
        $total=$cart->getPrice();
        $items=$cart->all();

      include('./view/home/order.html');

    }
}else{

    common::alertMes('请先登陆','login.php');
}





