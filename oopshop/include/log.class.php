<?php
class log{
    const LOGFILE='curr.log';//写日志
    public static function write($cont){
        $cont=date('Y-m-d h:i:s ',time()).$cont;
        $cont .= "\r\n";
        $log=self::isbak();//判断是否备份
        $fh=fopen($log,'a+');
        fwrite($fh,$cont);
        fclose($fh);
    }
    public static function bak(){
        $log=ROOT . 'data/log/' . self::LOGFILE;
        $bak=ROOT . 'data/log/' . date('ymd') . md5(uniqid(microtime(true),true)) . '.bak';
        return rename($log,$bak);
    }
    public static function isbak(){
        $log=ROOT . 'data/log/' . self::LOGFILE;
        if(!file_exists($log)){
            touch($log);
            return $log;
        }
        //判断大小
        clearstatcache(true,$log);
        $size=filesize($log);
        if($size <= 1024*1024){
            return $log;
        }
        //到这一步，说明>1M
        if(!self::bak()){
            return $log;
        }else{
            touch($log);
        }
    }
}