<?php
/**
 * @Author: Marte
 * @Date:   2017-07-10 10:17:53
 * @Last Modified by:   Marte
 * @Last Modified time: 2017-09-04 17:06:32
 */
// 验证码 缩略 2个水印


//
//生成验证码函数
//
class img{
    public function verifyimage($type=1,$length=4,$pixel=20,$line=5,$sess_name="verify"){


        $w=80;
        $h=28;
        $im=imagecreatetruecolor($w,$h);

        $white=imagecolorallocate($im,255,255,255);
        $black=imagecolorallocate($im,0,0,0);
        imagefilledrectangle($im,1,1,$w-2,$h-2,$white);
        $chars=buildrandomstring($type,$length);//建立随机字符串
        $_SESSION[$sess_name]=$chars;


        $fontfiles=Array('1.ttf','2.ttf','3.ttf');
        $fontcolor[]=imagecolorallocate($im,0x15,0x15,0x15);
        $fontcolor[]=imagecolorallocate($im,0x95,0x1e,0x04);
        $fontcolor[]=imagecolorallocate($im,0x93,0x14,0xa9);
        $fontcolor[]=imagecolorallocate($im,0x12,0x81,0x0a);
        $fontcolor[]=imagecolorallocate($im,0x06,0x3a,0xd5);
        for($i=0;$i<$length;$i++){
            $size=mt_rand(15,20);
            $angle=mt_rand(-10,10);
            $x=5+$i*$size;
            $y=mt_rand(20,26);

            $fontfile="../fonts/".$fontfiles[mt_rand(0,count($fontfiles)-1)];
            $color=imagecolorallocate($im,mt_rand(0,50),mt_rand(60,100),
                mt_rand(100,210));

            $text=substr($chars,$i,1);
            $color=$fontcolor[mt_rand(0,4)];
            imagettftext($im,$size,$angle,$x,$y,$color,$fontfile,$text);
        }

        if($pixel){
            for($i=0;$i<$pixel;$i++){
                imagesetpixel($im,mt_rand(0,$w-1),
                    mt_rand(0,$h-1),$black);
            }
        }

        if($line){
            for($i=1;$i<$line;$i++){
                $color=imagecolorallocate($im,mt_rand(0,70),
                    mt_rand(0,150),mt_rand(160,200));

                imageline($im,mt_rand(0,$w-1),mt_rand(0,$h-1),
                    mt_rand(0,$w-1),mt_rand(0,$h-1),$color);
            }
        }
        header("content-type:image/png");
        imagepng($im);
        imagedestroy($im);
    }
        //
    //缩略图函数
    //isreservedsource保留源
    //file_exists检查文件或目录是否存在
    //dirname()函数返回路径中的目录部分。
       public function thumb($filename,$destination=null,$dst_w=null,$dst_h=null,
                $isreservedsource=true,$scale=0.5){
                list($src_w,$src_h,$imagetype)=getimagesize($filename);

                if(is_null($dst_w)||is_null($dst_h)){
                    $dst_w=ceil($src_w*$scale);
                    $dst_h=ceil($src_h*$scale);
                }
                $mime=image_type_to_mime_type($imagetype);
                $createfun=str_replace("/","createfrom",$mime);
                $outfun=str_replace("/",null,$mime);
                $src_image=$createfun($filename);
                $dst_image=imagecreatetruecolor($dst_w,$dst_h);

                imagecopyresampled($dst_image,$src_image,0,0,0,0,$dst_w,$dst_h,$src_w,$src_h);
                if($destination && !file_exists(dirname($destination))){
                    mkdir(dirname($destination),0777,true);
                }

                $dstfilename=$destination==null?getuniname().".".getext($filename):$destination;
                $outfun($dst_image,$dstfilename);
                imagedestroy($src_image);
                imagedestroy($dst_image);
                if(!$isreservedsource){
                    unlink($filename);
                }
                return $dstfilename;
            }
    //
    //文字和图片水印函数
    //

        public function waterText($filename,$text="hello world",$fontfile="../fonts/1.ttf"){
            $fileinfo=getimagesize($filename);
            $mime=$fileinfo['mime'];
            $createfun=str_replace("/","createfrom",$mime);
            $outfun=str_replace("/", null,$mime);
            $im=$createfun($filename);

            //为一幅图像分配颜色 + alpha(0~127透明度);
            $color=imagecolorallocatealpha($im,255,0,0,50);
            $fontfile="../fonts/{$fontfile}";
            imagettftext($im,14,0,0,14,$color,$fontfile,$text);
            $outfun($im,$filename);
            imagedestroy($im);

            }


        public function waterPic($dstfile,$srcfile="../images/webLogo.jpg",$pct=30){
            $srcfileinfo=getimagesize($srcfile);//获取图片的尺寸
            $src_w=$srcfileinfo[0];
            $src_h=$srcfileinfo[1];

            $dstfileinfo=getimagesize($dstfile);
            $srcmime=$srcfileinfo['mime'];
            $dstmime=$dstfileinfo['mime'];
            //str_replace(find,replace,string,count);
            $createSrcFun=str_replace("/","createfrom",$srcmime);
            $createDstFun=str_replace("/","createfrom",$dstmime);
            $outdstfun=str_replace("/",null,$dstmime);
            $dst_im=$createDstFun($dstfile);
            $src_im=$createSrcFun($srcfile);
            imagecopymerge($dst_im,$src_im,0,0,0,0,$src_w,$src_h,$pct);
            $outdstfun($dst_im,$dstfile);
            imagedestroy($src_im);
            imagedestroy($dst_im);

        }


}






