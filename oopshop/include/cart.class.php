<?php

class cart{
    private static $ins=null;
    private $items=array();

    final public function __construct(){
    }
    final protected function __clone(){
    }
    protected static function getIns(){
        if(!(self::$ins instanceof self)){
            self::$ins=new self();
        }
        return self::$ins;
    }
    public static function getCart(){
        if(!isset($_SESSION['cart'])||!($_SESSION['cart'] instanceof self)){
            $_SESSION['cart']=self::getIns();
        }
        return $_SESSION['cart'];
    }
    //添加商品
    public function addItem($id,$name,$price,$num=1){
        if($this->hasItem($id)){//若该商品已存在，则直接加其数量
            $this->incNum($id,$num);
            return;
        }
        $item=array();
        $item['name']=$name;
        $item['price']=$price;
        $item['num']=$num;
        $this->items[$id]=$item;
    }
    //修改购物车商品数量
    public function modNum($id,$num=1){
        if(!$this->hasItem($id)){
            return false;
        }
        $this->items[$id]['num']=$num;
    }
    //商品数量加1
    public function incNum($id,$num=1){
        if($this->hasItem($id)){
            $this->items[$id]['num'] += $num;
        }
    }
    //商品数量减1
    public function decNum($id,$num=1){
        if($this->hasItem($id)){
            $this->items[$id]['num'] -= $num;
        }
        //如果减少后，数量为0，则把这个商品从购物车中删掉
        if($this->items[$id]['num']<1){
            $this->delItem($id);
        }
    }
    //判断商品是否存在
    public function hasItem($id){
        return array_key_exists($id,$this->items);
    }
    //删除商品
    public function delItem($id){
        unset($this->items[$id]);
    }
    //查询购物车中商品的种类
    public function getCnt(){
        return count($this->items);
    }
    //查询购物车中商品的个数
    public function getNum(){
        if($this->getCnt() == 0){
            return 0;
        }
        $sum=0;
        foreach ($this->items as $item) {
            $sum += $item['num'];
        }
        return $sum;
    }
    //查询购物车中商品的总金额
    public function getPrice(){
        if($this->getCnt() == 0){
            return 0;
        }
        $price=0.0;
        foreach ($this->items as $item) {
            $price += $item['num'] * $item['price'];
        }
        return $price;
    }
    //返回购物车中的所有商品
    public function all(){
        return $this->items;
    }
    //清空购物车
    public function clear(){
        $this->items=array();
    }
}




