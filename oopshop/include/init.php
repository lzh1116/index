<?php
define('ROOT',str_replace('\\','/',dirname(dirname(__FILE__))) . '/');

session_start();

function __autoload($class){

    if(strtolower(substr($class,-5))=='model'){
        require ROOT.'model/'.$class.'.class.php';

    }else{

        require ROOT.'include/'.$class.'.class.php';
    }


}



